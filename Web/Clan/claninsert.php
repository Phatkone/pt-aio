<?php
/** 
*	     ClanInsert PHP Script
*	Written By EuphoriA / Phatkone
**/
include '..\\ServerMain\\settings.php';
if (!defined("PT"))
	die();
if ($dbconn) 
{
	$userid = isset($userid) ? $userid : "";
	$gserver = isset($gserver) ? $gserver : "";
	$chname = isset($chname) ? $chname : "";
	$clname = isset($clName) ? $clName : "";
	$chtype = isset($chtype) ? $chtype : "";
	$lv = isset($lv) ? $lv : "";
	$ticket = isset($ticket) ? $ticket : "";
	$expl = (defined('expl')) ? expl : "";
	if ($userid=="" || $gserver =="" || $chname=="" || $clname=="" || $chtype=="" || $lv=="" || $ticket=="") 
	{
		print("Code=102".$CR);
		die;
	}
	$ctcheck = "SELECT SNo FROM clandb.dbo.CT WHERE ChName='$chname' AND UserID='$userid'";
	$stmt = $dbconn->prepare($ctcheck);
	$stmt->execute();
	$tticket1 = $stmt->fetch(PDO::FETCH_ASSOC);
	if ($ticket != $tticket1['SNo']) 
	{
		print("Code=101".$CR);
		$dbconn = null;
		die;
	} 
	$clancheck = "SELECT ClanName FROM clandb.dbo.UL WHERE ChName='$chname'";
	$stmt = $dbconn->prepare($clancheck);
	$stmt->execute();
	$clancheck1 = $stmt->fetch(PDO::FETCH_ASSOC);
	print_r($clancheck1);
	if (is_array($clancheck1))
	{
		print("Code=2".$CR."CMoney=0".$CR);
		$dbconn = null;
		die;
	}
	$leader = "SELECT ClanZang FROM clandb.dbo.CL WHERE ClanName='$clname'";
	$stmt = $dbconn->prepare($leader);
	$stmt->execute();
	$leadercheck1 = $stmt->fetch();
	if ($leadercheck1[0] != "") {
		sqlsrv_close($dbconn);
		print("Code=3".$CR."CMoney=0".$CR);
		die;
	}
	
	$LI = "SELECT IMG FROM clandb.dbo.LI WHERE ID=1";
	$stmt = $dbconn->prepare($LI);
	$stmt->execute();
	$IMG1 = $stmt->fetch();
	if ($IMG1[0] != "") 
	{
		$iIMG = $IMG1[0];
	} 
	else 
	{
		$iIMG = 100000000;
		$imginsert = "INSERT INTO clandb.dbo.LI ('$iIMG','1')";
		$stmt = $dbconn->prepare($imginsert);
		$stmt->execute();
	}
	$iIMG = $iIMG + 1;
	if (!is_file("..\\ClanContent\\$iIMG.bmp"))
	{
		copy("..\\ClanContent\\ClanImgTemplate.bmp","..\\ClanContent\\$iIMG.bmp");
	}
	$imginsert = "UPDATE clandb.dbo.LI SET IMG='$iIMG' WHERE ID=1";
	$stmt = $dbconn->prepare($imginsert);
	$stmt->execute();
	$IDX = "SELECT MAX(IDX) FROM clandb.dbo.CL";
	$stmt = $dbconn->prepare($IDX);
	$stmt->execute();
	$IDX2 = $stmt->fetch();
	if ($IDX2[0] != "") 
	{
		$iIDX = $IDX2[0];
	}
	$iIDX = @$iIDX + 1;
	$edate = date('Y-m-d', strtotime('+20 years'));
	/* Check if IDX Value auto increments in CL table */
	$autoidxquery = "SELECT is_identity FROM sys.columns WHERE object_id = object_id('clandb.dbo.cl') AND name = 'IDX'";
	$stmt = $dbconn->prepare($autoidxquery);
	$autoidxresult = $stmt->fetch();
	if ($autoidxresult[0] == 1)
	{
		$CLInsert = "INSERT INTO clandb.dbo.CL 
		([ClanName],[UserID],[ClanZang],[MemCnt],[Note],[MIconCnt],[RegiDate],[LimitDate],[DelActive],[PFlag],[KFlag],[Flag],[NoteCnt],[Cpoint],[CWin],[CFail],[ClanMoney],[CNFlag],[SiegeMoney])
		VALUES
		('$clname','$userid','$chname','1','$expl','$iIMG',GETDATE(),'$edate','0','0','0','0','1','0','0','0','0','0','0')";
	}
	else
	{
		$CLInsert = "INSERT INTO clandb.dbo.CL 
		([IDX],[ClanName],[UserID],[ClanZang],[MemCnt],[Note],[MIconCnt],[RegiDate],[LimitDate],[DelActive],[PFlag],[KFlag],[Flag],[NoteCnt],[Cpoint],[CWin],[CFail],[ClanMoney],[CNFlag],[SiegeMoney]) 
		VALUES 
		('$iIDX','$clname','$userid','$chname','1','$expl','$iIMG',GETDATE(),'$edate','0','0','0','0','1','0','0','0','0','0','0')";
	}
	$stmt = $dbconn->prepare($CLInsert);
	$stmt->execute();
	$IDXCheck = "SELECT IDX FROM clandb.dbo.CL WHERE ClanName='$clname'";
	$stmt = $dbconn->prepare($IDXCheck);
	$stmt->execute();
	$IDXC1 = $stmt->fetch();
	if ($IDXC1[0] != "") 
	{
		$IDX = $IDXC1[0];
	}
	$autoidxquery = "SELECT is_identity FROM sys.columns WHERE object_id = object_id('clandb.dbo.ul') AND name = 'IDX'";
	$stmt = $dbconn->prepare($autoidxquery);
	$stmt->execute();
	$autoidxresult = $stmt->fetch();
	if ($autoidxresult[0] == 1)
	{
		$ULInsert = "INSERT INTO clandb.dbo.UL 
		([MIDX],[userid],[ChName],[ClanName],[ChType],[ChLv],[Permi],[JoinDate],[DelActive],[PFlag],[KFlag],[MIconCnt]) 
		VALUES 
		('3','$userid','$chname','$clname','$chtype','$lv','0',GETDATE(),'0','0','0','$iIMG')";
	}
	else
	{
		$ULInsert = "INSERT INTO clandb.dbo.UL 
		([IDX],[MIDX],[userid],[ChName],[ClanName],[ChType],[ChLv],[Permi],[JoinDate],[DelActive],[PFlag],[KFlag],[MIconCnt]) 
		VALUES 
		('$IDX','3','$userid','$chname','$clname','$chtype','$lv','0',GETDATE(),'0','0','0','$iIMG')";
	}
	$stmt = $dbconn->prepare($ULInsert);
	$stmt->execute();
	print("Code=1".$CR."CMoney=500000".$CR);
	$dbconn = null;
} 
else 
{

	$dbconn = null;
	print("Code=103".$CR);
	die;
}
?>