<?php
/**
*  Clan SoD Score - Written By Phatkone/EuphoriA
*  Credit to Vormav for Query Strings
**/
include '..\\ServerMain\\settings.php';
if (!defined("PT"))
	die();
$SNo = isset($SNo) ? $SNo : "";
$yyMM = date('ym');
$today = new DateTime();
$today = $today->format('m-d-y H:i');

$addtime = new DateTime();
$negtime = new DateTime();
$addtime->add(new DateInterval('PT' . 2 . 'M'));
$negtime->sub(new DateInterval('PT' . 2 . 'M'));
$addtime = $addtime->format('m-d-y H:i:s');
$negtime = $negtime->format('m-d-y H:i:s');

if ($dbconn) 
{
	$query = "INSERT INTO soddb.dbo.sod2clan$yyMM 
				SELECT u.ChName, r.Point, r.RegistDay, u.ClanName 
				FROM clandb.dbo.ul u, soddb.dbo.sod2record$yyMM r
				WHERE u.ChName = r.CharName
				AND r.SNo = '$SNo' 
				AND r.RegistDay 
				BETWEEN '$negtime' AND '$addtime'";
	
	$stmt = $dbconn->prepare($query);
	$stmt->execute();
	
	$query2 = "	SELECT u.ChName, r.Point, r.RegistDay, u.ClanName
					FROM clandb.dbo.UL u, soddb.dbo.sod2record$yyMM r 
					WHERE u.ChName = r.CharName
					AND r.RegistDay BETWEEN '$negtime' AND '$addtime' 
					AND u.ClanName = (SELECT u.ClanName 
										FROM ClanDB.dbo.UL u
										INNER JOIN soddb.dbo.sod2record$yyMM r 
											ON r.SNo = '$SNo' 
											AND u.ChName = r.CharName 
										WHERE
										u.ClanName=(SELECT u.ClanName 
													FROM clandb.dbo.UL u 
													INNER JOIN soddb.dbo.sod2record$yyMM r 
														ON r.SNo = '$SNo' 
														AND u.ChName = r.CharName))";
	
	$stmt = $dbconn->prepare($query2);
	$stmt->execute();
	$results = $stmt->fetchAll();
	if (count($results) >= 2) 
	{
		$query3 = "UPDATE ClanDb.dbo.CL 
					SET RegiDate = '$today', 
						Cpoint = (SELECT SUM(Point)
									FROM SodDb.dbo.Sod2Clan$yyMM s 
									WHERE s.RegistDay BETWEEN '$negtime' AND '$addtime' 
									AND	ClanName = (SELECT u.ClanName 
														FROM ClanDB.dbo.UL u 
														INNER JOIN soddb.dbo.sod2record$yyMM r 
														ON r.SNo = '$SNo' 
														AND u.ChName = r.CharName)) 
					WHERE ClanName=(SELECT u.ClanName 
										FROM clandb.dbo.UL u 
										INNER JOIN soddb.dbo.sod2record$yyMM r 
										ON r.SNo = '$SNo' 
										AND	u.ChName = r.CharName) 
					AND ClanDB.dbo.CL.Cpoint <= (SELECT SUM(Point)
													FROM SodDb.dbo.sod2clan$yyMM s 
													WHERE s.RegistDay BETWEEN '$negtime' AND '$addtime' 
													AND ClanName = (SELECT u.ClanName 
																		FROM clandb.dbo.ul u
																		INNER JOIN soddb.dbo.sod2record$yyMM r 
																		ON r.SNo = '$SNo' 
																		AND u.ChName = r.CharName))"; 
		
		$stmt = $dbconn->prepare($query3);
		$stmt->execute();
		print "Bellatra";
	}
	$dbconn = null;
} 
else 
{
	print "Unable to Connect";
}
?>
