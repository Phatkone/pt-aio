<?php
/**
* Written by EuphoriA
**/
include 'settings.php';
if (!defined("PT"))
	die();
$chname = (isset($chname)) ? $chname : "";
$gserver = (isset($gserver)) ? $gserver : "";
$clName = (isset($clName)) ? $clName : "";
$userid = (isset($userid)) ? $userid : "";
$clwon = (isset($clwon)) ? $clwon : "";
$clwonUserid = (isset($clwonUserid)) ? $clwonUserid : "";
$lv = (isset($lv)) ? $lv : "";
$chtype = (isset($chtype)) ? $chtype : ""; 
$chlv = (isset($chlv)) ? $chlv : "";
$chipflag = (isset($chipflag)) ? $chipflag : "";
$date = date('m-d-y');
if ($chname == ""|| $gserver == "" || $clName == "" || $userid == "" || $clwon == "" || $clwonUserid == "" || $lv == "" || $chtype == "" || $chlv == "" || $chipflag == "")
{
	$dbconn = null;
	die("Code=100$CR");
}
$query = "SELECT IDX,ClanZang,MemCnt 
			FROM ClanDB.dbo.CL 
			WHERE ClanName='$clName'";
$stmt = $dbconn->prepare($query);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (count($result) >= 1)
{
	$ClanLeader = $result['ClanZang'];
	$MemCount = $result['MemCnt'];
	$IDX = $result['IDX'];
}
else
{
	$dbconn = null;
	die("Code=0$CR");
}
if (((int)$MemCount +1) > 100)
{
	$dbconn = null;
	die("Code=2$CR");
}
$query = "SELECT ChName 
			FROM ClanDB.dbo.UL 
			WHERE Permi=2 
			AND ClanName='$clName'";
$stmt = $dbconn->prepare($query);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (count($result) >= 1)
{
	$SubChief = $result['ChName'];
}
else
{
	$SubChief = "";
}
if ($ClanLeader != $chname && (string)$SubChief != $chname)
{
	$dbconn = null;
	die("Code=0$CR");
}
$query = "SELECT ClanName 
			FROM ClanDB.dbo.UL
			WHERE ChName='$clwon'";
$stmt = $dbconn->prepare($query);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (count($result) >= 1)
{
	$uclname = $result['ClanName'];
}
else
{
	$uclname = "";
}
if ((string)$uclname != "")
{
	$dbconn = null;
	die("Code=0$CR");
}
else
{
	if ((string)$uclname == "" && sqlsrv_num_rows($result) >= 1)
	{
		$query = "DELETE 
					FROM ClanDB.dbo.UL 
					WHERE ChName='$clwon'";
		$stmt = $dbconn->prepare($query);
		$stmt->execute();
	}
}
$iMIDX = "";
$query = "SELECT MAX(MIDX) 
			FROM ClanDB.dbo.UL";
$stmt = $dbconn->prepare($query);
$stmt->execute();
$result = $stmt->fetch(PDO::FETCH_ASSOC);
if (count($result) >= 1)
{
	$iMIDX = $result['MIDX'];
}
$iMIDX++;
$MemCount++;
$query = "UPDATE ClanDB.dbo.CL 
			SET MemCnt='$MemCount' 
			WHERE ClanName='$clName'";
$stmt = $dbconn->prepare($query);
$stmt->execute();
$query = "INSERT INTO ClanDB.dbo.UL 
			([IDX],[MIDX],[userid],[ChName],[ClanName],[ChType],[ChLv],[Permi],[JoinDate],[DelActive],[PFlag],[KFlag],[MIconCnt]) 
			VALUES 
			('$IDX','$iMIDX','$clwonUserid','$clwon','$clName','$chtype','$chlv','0',$date,'0','0','0','0')";
$stmt = $dbconn->prepare($query);
$stmt->execute();
$dbconn = null;
print("Code=1$CR");
?>