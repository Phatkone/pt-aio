<?php
/**
* Written by EuphoriA
**/
include 'settings.php';
if ($dbconn)
{
	$CR = chr(13);
	(int)$num = (isset($_GET['num'])) ? $_GET['num'] : "";
	if ($num == "" && $num > 1000000001)
	{
		die("Code=100$CR");
	}
	$query = "SELECT ClanName,Note FROM Clandb.dbo.CL WHERE MIconCnt='$num'";
	$stmt = $dbconn->prepare($query);
	$stmt->execute();
	$result = $stmt->fetch(PDO::FETCH_ASSOC);
	if (!$result) 
	{
		print("Code=0$CR");
		$dbconn = null;
		die;
	}
	if ($result == false || count($result) <= 0)
	{
		print("Code=0$CR");
	}
	else
	{
		$ClanName = $result['ClanName'];
		$ClanNote = $result['Note'];
		print("Code=1 $CR CName=$ClanName $CR CNote=$ClanNote$CR");
	}
	$dbconn = null;
}
else
{
	die("Code=100$CR");
}
?>