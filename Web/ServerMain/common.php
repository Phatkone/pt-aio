<?php
try 
{
	$dbconn = new PDO("sqlsrv:server=$server", $UID, $PWD);
}
catch (PDOException $e)
{
	echo "Connection Failed: ". $e->getMessage();
	$dbconn = false;
} 
$expl = "PristonTale Clan";
/**
*
*	No changes to be made beyond this point.
*
**/
define('PT',1);
define('expl', $expl); /**  Setting to the constant here so it can't be overwritten by the game in the $_GET request.  **/
$CR = chr(13);
$uri = urldecode($_SERVER['REQUEST_URI']);
$gs = substr($uri, strpos($uri,'?')+1);
$uri = [];
$ex = explode('&',$gs);
foreach ($ex as $g)
{
	$e = explode('=',$g);
	if (strlen($e[0]) > 0 && strlen($e[1]) > 0)
	{

		$uri[cleanStr($e[0])] = cleanStr($e[1]);
	}
}
foreach ($uri as $k => $v)
{
	$$k = $v;
}
unset($ex, $e, $g, $gs, $k, $v);

function cleanStr($str)
{
	$cs = "";
	$cleanchars = 'abcdefghijklmnopqrstuvwxyz0123456789-[]';
	for ($i = 0; $i < strlen($str); $i++)
	{
		if (stripos($cleanchars, $str[$i]) !== false)
		{
			$cs .= $str[$i];
		}
	}
	return $cs;
}
?>