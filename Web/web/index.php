<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));

require_once('./common/pages.class.php');
require_once('./common/login.php');
global $admin;
?>
<html lang='en'>
<head>
<title>
    Pristontale Website and Database Setup | By Phatkone
</title>
<link rel='stylesheet' href='/web/css/jquery-ui.min.css' />
<link rel='stylesheet' href='/web/css/web.css' />
<script type='text/javascript' src='/web/js/jquery-2-1-1.min.js'></script>
<script type='text/javascript' src='/web/js/jquery-ui.min.js'></script>
<script type='text/javascript' src='/web/js/web.js'></script>
<?= ($admin && $verified)?"<script type='text/javascript' src='/web/js/admin.js'></script>":"";?>
<link rel='icon' type='image/x-icon' href='/web/images/favicon.ico' />
<link rel='shortcut icon' type='image/x-icon' href='/web/images/favicon.ico' />
</head>
<body class='<?=$theme?>'>
<?php
require_once('common/menu.php'); 
echo $loginMsg;
if (!isset($sub[0]))
{
    pages::render('home','php');
}
else
{
    pages::render($sub[0],'php');
}
?>
<div>
    <div class='body third'><h3>Server:</h3>
        <div style='display:inline-block;margin-left:-20%;padding-right:5%;vertical-align:top;margin-top:7.5%;'><img src='/web/images/status.png' style='height:50px;width:50px' /></div>
        <div style='display:inline-block'><?php include('common/server.php'); ?></div>
    </div>
    <div class='body third'><h3>Crowns:</h3>
        <div style='display:inline-block;margin-left:-20%;padding-right:5%;vertical-align:top;margin-top:7.5%;'><img src='/web/images/crown.png' style='height:50px;width:50px' /></div>
        <div style='display:inline-block'><?php include('common/crowns.php'); ?></div>
    </div>
    <div class='body third'><h3>Top 5:</h3>
        <div style='display:inline-block;margin-left:-20%;padding-right:5%;vertical-align:top;margin-top:7.5%;'><img src='/web/images/top5.png' style='height:50px;width:50px' /></div>
        <div style='display:inline-block'><?php include('common/top5.php'); ?></div>
    </div>
</div>
<footer class='footer'>All rights reserved &copy Kilroy Inc</footer>