<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
$online = (is_resource(@fsockopen($configs['serverAddress'],$configs['serverPort'],$errno, $errstr,3))) ? "<font color='#00CC00'>Online</font>": "<font color='#CC0000'>Offline</font>";
echo "<p>Status: {$online}</p>";
$time = date("H:i");
echo "<p>Server Time: {$time}</p>";
if ($db->Connect() === true)
{
    $bt = $db->query("SELECT TOP(1) bosstime FROM [GameDB].[dbo].[BossTime] ORDER BY date DESC");
    if (empty($bt))
        echo "<p>Bosstime: Unknown</p>";
    else
    {
        $boss = str_ireplace("XX", date("H"), $bt[0]['bosstime']);
        echo "<p>Bosstime: {$boss}</p>";
    }

}
else
{
    print("Database Connection Issue :/");
}
?>