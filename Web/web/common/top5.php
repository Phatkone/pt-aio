<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
if ($db->Connect() === true)
{
    $top5 = $db->query("SELECT TOP(5) CharName, CharLevel FROM [GameDB].[dbo].[LevelList] WHERE CharName NOT LIKE '\[GM\]%' ESCAPE '\' AND CharName NOT LIKE '\[ADM\]%' ESCAPE '\' ORDER BY Exp DESC");
    if (empty($top5))
        echo "<p>No Characters Yet</p>";
    else
    {
        echo "<table>";
        $count = 1;
        foreach ($top5 as $player)
        {
            echo "<tr><td>#{$count}</td><td>{$player['CharName']}</td><td>{$player['CharLevel']}</td></tr>";
            $count++;
        }
        echo "</table>";
    }

}
else
{
    print("Database Connection Issue :/");
}
?>