<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));

$page = (isset($page)) ? $page : "";
$loggedin = (isset($loggedin)) ? $loggedin : false;
$admin = (isset($admin)) ? $admin : false;
?>
<div class='menu'>
	<a href='?'><div class='menubutton<?=($page == 'home' || $page == "") ? ' active' : "" ?>'>Home</div></a>
	<?php
	if (!$verified)
	{
	?>
		<div class='menubutton dropdown<?=($page == 'account') ? ' active' : "";?>'>Account
			<div class='submenu'>
					<div class='menubutton' id='login'>Login</div>
					<a href='?account::register'><div class='menubutton'>Register</div></a>
					<?php //($configs['verification']) ? "<a href='?account::recover'><div class='menubutton'>Forgot Password</div></a>" : "";//Put in accounnt recovery?>
			</div>
		</div>
	<?php 
	}
	?>
	<div class='menubutton dropdown <?=($page == 'ranks') ? ' active' : "";?>'>Ranks
		<div class='submenu'>
			<a href='?ranks::clan'><div class='menubutton' id='clanranks'>Clan Ranks</div></a>
			<a href='?ranks::player'><div class='menubutton' id='playerranks'>Player Ranks</div></a>
			<a href='?ranks::sod/clan'><div class='menubutton' id='clansod'>Clan SOD Ranks</div></a>
			<a href='?ranks::sod/player'><div class='menubutton' id='playersod'>Player SOD Ranks</div></a>
		</div>
	</div>
	<div class='menubutton dropdown <?=($page == 'info') ? ' active' : "";?>'>Server Info
		<div class='submenu'>
			<div class='menubutton dropdown' id='weapons'>Weapons
				<div class='submenu2'>
					<a href='?info::items/axes'><div class='menubutton' id='axes'>Axes</div></a>
					<a href='?info::items/bows'><div class='menubutton' id='bows'>Bows</div></a>
					<a href='?info::items/claws'><div class='menubutton' id='claws'>Claws</div></a>
					<a href='?info::items/daggers'><div class='menubutton' id='daggers'>Daggers</div></a>
					<a href='?info::items/hammers'><div class='menubutton' id='hammers'>Hammers</div></a>
					<a href='?info::items/javelins'><div class='menubutton' id='javelins'>Javelins</div></a>
					<a href='?info::items/phantoms'><div class='menubutton' id='phantoms'>Phantoms</div></a>
					<a href='?info::items/scythes'><div class='menubutton' id='scythes'>Scythes</div></a>
					<a href='?info::items/swords'><div class='menubutton' id='swords'>Swords</div></a>
					<a href='?info::items/vambraces'><div class='menubutton' id='vambraces'>Vambraces</div></a>
					<a href='?info::items/wands'><div class='menubutton' id='wands'>Wands</div></a>
				</div>
			</div>
			<div class='menubutton dropdown' id='defences'>Defence and Accessories
				<div class='submenu2'>
					<a href='?info::items/amulets'><div class='menubutton' id='amulets'>Amulets</div></a>
					<a href='?info::items/armors'><div class='menubutton' id='armors'>Armors</div></a>
					<a href='?info::items/armlets'><div class='menubutton' id='armlets'>Armlets</div></a>
					<a href='?info::items/boots'><div class='menubutton' id='boots'>Boots</div></a>
					<a href='?info::items/gauntlets'><div class='menubutton' id='gauntlets'>Gauntlets</div></a>
					<a href='?info::items/orbs'><div class='menubutton' id='orbs'>Orbs</div></a>
					<a href='?info::items/rings'><div class='menubutton' id='rings'>Rings</div></a>
					<a href='?info::items/robes'><div class='menubutton' id='robes'>Robes</div></a>
					<a href='?info::items/shields'><div class='menubutton' id='shields'>Shields</div></a>
					<a href='?info::items/sheltoms'><div class='menubutton' id='sheltomes'>Sheltoms</div></a>
				</div>
			</div>
			<a href='?info::dropsearch'><div class='menubutton' id='dropsearch'>Item Drop Search</div></a>
		</div>
	</div>
	<a href='game.7z' download><div class='menubutton'>Download</div></a>
	
	<?php
	if ($admin && $verified)
	{
	?>
		<div class='menubutton dropdown <?=($page == 'admin') ? ' active' : "";?>'>Admin
			<div class='submenu'>
				<a href='?admin::currentplayers'><div class='menubutton' id='currentplayers'>Current Players</div></a>
				<a href='?admin::giftserver'><div class='menubutton' id='giftserver'>Item Distributor</div></a>
				<a href='?admin::accountblock'><div class='menubutton' id='accountblock'>Account Block</div></a>
				<a href='?admin::setup'><div class='menubutton' id='setup'>Setup</div></a>
			</div>
		</div>
	<?php
	}
	if ($verified)
	{
	?>
		<div class='menubutton dropdown username<?=($page == 'account') ? ' active' : "";?>'><?=ucwords($uname);?>
			<div class='submenu'>
				<a href='?account::settings'><div class='menubutton' id='settings'>Settings</div></a>
				<a href='?account::logout'><div class='menubutton' id='logout'>Logout</div></a>
				<!-- Add Clan Management Here (Icon, Note) -->
			</div>
		</div>
	<?php
	}
	?>
</div>
<div id='loginbgdiv'>
	<div id='logindiv'>
		<form method='POST'>
			<h2>Login</h2>
			<table>
				<tr><td>Username:</td><td><input name='uname' type='text' /></td></tr>
				<tr><td>Password:</td><td><input name='upwd' type='password' /></td></tr>
				<tr><td colspan='2'><input type='submit' value='login'/></td></tr>
			</table>
		</form>
	</div>
</div>