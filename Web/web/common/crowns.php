<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
if ($db->Connect() === true)
{
    $sod = $db->query("SELECT TOP(1) ClanName FROM [ClanDB].[dbo].[CL] ORDER BY CPoint DESC");
    $bc = $db->query("SELECT TOP(1) b.ClanName FROM [GameDB].[dbo].[Bless] a JOIN [ClanDB].[dbo].[CL] b ON (a.miconcnt = b.miconcnt) ORDER BY a.date DESC");
    if (empty($sod))
        echo "<p>SOD: None</p>";
    else
        echo "<p>SOD: {$sod[0]['ClanName']}</p>";
    if (empty($bc))
        echo "<p>BC: None</p>";
    else
        echo "<p>BC: {$bc[0]['ClanName']}</p>";

}
else
{
    print("Database Connection Issue :/");
}
?>