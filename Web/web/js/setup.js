$(document).ready(function() {
    if ($('#emailChecked').is(":checked")) 
    {
        $('.email').show();
    }
    else
    {
        $('.email').hide();
    }
    $('#emailChecked').change(function() {
        if ($(this).is(":checked"))
        {
            $('.email').show();
        }
        else
        {
            $('.email').hide();
        }
    });
    $('.stop').click(function(e) {
        e.preventDefault();
    });
    $('select[name="theme"]').change(function() {
        var theme = $(this)[0].value;
        $('body').attr('class',theme);
    });
});

function makeDB(db, usr, id) 
{
    uri = `?setup::step/2/action/checkDB/${db}/${usr}`
    $.ajax({
        url: uri,
        method: 'POST',
        data: {},
        dataType: 'JSON',
        async: false,
        error: function(data) {
            alert("Uh oh, error checking database " + db + "\r\n Check error log for more information");
        }
    }).then(function (r) {
        if (r.hasOwnProperty('Success'))
        {
            if (r.Success == "Exist")
            {
                td = `#dbt${id}`;
                $(td).css('background-color', '#44dd44');
                $(td).text(`${db} Already Exists`);
            }
            else
            {
                uri = `?setup::step/2/action/makeDB/${db}/${usr}`;
                $.ajax({
                    url: uri,
                    method: 'POST',
                    data: {},
                    dataType: 'JSON',
                    async: false,
                    success: function(r) {
                        td = `#dbt${id}`;
                        if (r.hasOwnProperty('Success'))
                        {
                            $(td).text(r.Success);
                            $(td).css('background-color', '#44dd44');
                        }
                        else if (r.hasOwnProperty('Error'))
                        {
                            $(td).text(r.Error);
                            $(td).css('background-color', '#dd4444');
                        }
                    },
                    error: function(data) {
                        alert("Uh oh, error making database " + db + "\r\n Check error log for more information");
                    }
                });
            }
        }
        else if (r.hasOwnProperty('Error'))
        {
            console.log("Error: " + r.Error);
        }
    });
}

function makeUser(usr, pwd, id) 
{
    uri = `?setup::step/2/action/checkUser/${usr}`
    $.ajax({
        url: uri,
        method: 'POST',
        data: {},
        dataType: 'JSON',
        async: false,
        error: function(data) {
            alert("Uh oh, error checking user " + usr + "\r\n Check error log for more information");
        }
    }).then(function (r) {
        if (r.hasOwnProperty('Success'))
        {
            if (r.Success == "Exist")
            {
                td = `#dbt${id}`;
                $(td).css('background-color', '#44dd44');
                $(td).text(`${usr} Already Exists`);
            }
            else
            {
                uri = `?setup::step/2/action/makeUser/${usr}/${pwd}`;
                $.ajax({
                    url: uri,
                    method: 'POST',
                    data: {},
                    dataType: 'JSON',
                    async: false,
                    success: function(r) {
                        td = `#dbt${id}`;
                        if (r.hasOwnProperty('Success'))
                        {
                            $(td).text(r.Success);
                            $(td).css('background-color', '#44dd44');
                        }
                        else if (r.hasOwnProperty('Error'))
                        {
                            $(td).text(r.Error);
                            $(td).css('background-color', '#dd4444');
                        }
                    },
                    error: function(data) {
                        alert("Uh oh, error making user " + usr + "\r\n Check error log for more information");
                    }
                });
            }
        }
        else if (r.hasOwnProperty('Error'))
        {
            console.log("Error: " + r.Error);
        }
    });
}

function makeTable(db, tbl, id) 
{
    uri = `?setup::step/2/action/checkTable/${db}/${tbl}`
    $.ajax({
        url: uri, 
        method: 'POST',
        data: {},
        dataType: 'JSON',
        async: false,
        error: function(data) {
            alert("Uh oh, error checking table with " + tbl + "\r\n Check error log for more information");
        }
    }).then(function (r) {
        if (r.hasOwnProperty('Success'))
        {
            if (r.Success == "Exist")
            {
                td = `#dbt${id}`;
                $(td).css('background-color', '#44dd44');
                $(td).text(`${tbl} Already Exists`);
            }
            else
            {
                uri = `?setup::step/2/action/makeTable/${db}/${tbl}`;
                $.ajax({
                    url: uri,
                    method: 'POST',
                    data: {},
                    dataType: 'JSON',
                    async: false,
                    success: function(r) {
                        td = `#dbt${id}`;
                        if (r.hasOwnProperty('Success'))
                        {
                            $(td).text(r.Success);
                            $(td).css('background-color', '#44dd44');
                        }
                        else if (r.hasOwnProperty('Error'))
                        {
                            $(td).text(r.Error);
                            $(td).css('background-color', '#dd4444');
                        }
                    },
                    error: function(data) {
                        alert("Uh oh, error making table with " + tbl + "\r\n Check error log for more information");
                    }
                });
            }
        }
        else if (r.hasOwnProperty('Error'))
        {
            console.log("Error: " + r.Error);
        }
    });
}

function makeView(db, view, id) 
{
    uri = `?setup::step/2/action/checkView/${db}/${view}`
    $.ajax({
        url: uri,
        method: 'POST',
        data: {},
        dataType: 'JSON',
        async: false,
        error: function(data) {
            alert("Uh oh, error checking view with " + view + "\r\n Check error log for more information");
        }
    }).then(function (r) {
        if (r.hasOwnProperty('Success'))
        {
            if (r.Success == "Exist")
            {
                td = `#dbt${id}`;
                $(td).css('background-color', '#44dd44');
                $(td).text(`${view} Already Exists`);
            }
            else
            {
                uri = `?setup::step/2/action/makeTable/${db}/${view}`;
                $.ajax({
                    url: uri,
                    method: 'POST',
                    data: {},
                    dataType: 'JSON',
                    async: false,
                    success: function(r) {
                        td = `#dbt${id}`;
                        if (r.hasOwnProperty('Success'))
                        {
                            $(td).text(r.Success);
                            $(td).css('background-color', '#44dd44');
                        }
                        else if (r.hasOwnProperty('Error'))
                        {
                            $(td).text(r.Error);
                            $(td).css('background-color', '#dd4444');
                        }
                    },
                    error: function(data) {
                        alert("Uh oh, error making view with " + view + "\r\n Check error log for more information");
                    }
                });
            }
        }
        else if (r.hasOwnProperty('Error'))
        {
            console.log("Error: " + r.Error);
        }
    });
}

function makeProcedure(db, proc, id) 
{
    uri = `?setup::step/2/action/checkProcedure/${db}/${proc}`
    $.ajax({
        url: uri,
        method: 'POST',
        data: {},
        dataType: 'JSON',
        async: false,
        error: function(data) {
            console.log(data);
            alert("Uh oh, error checking procedure with " + proc + "\r\n Check error log for more information");
        }
    }).then(function (r) {
        if (r.hasOwnProperty('Success'))
        {
            if (r.Success == "Exist")
            {
                td = `#dbt${id}`;
                $(td).css('background-color', '#44dd44');
                $(td).text(`${proc} Already Exists`);
            }
            else
            {
                uri = `?setup::step/2/action/makeTable/${db}/${proc}`;
                $.ajax({
                    url: uri,
                    method: 'POST',
                    data: {},
                    dataType: 'JSON',
                    success: function(r) {
                        td = `#dbt${id}`;
                        if (r.hasOwnProperty('Success'))
                        {
                            $(td).text(r.Success);
                            $(td).css('background-color', '#44dd44');
                        }
                        else if (r.hasOwnProperty('Error'))
                        {
                            $(td).text(r.Error);
                            $(td).css('background-color', '#dd4444');
                        }
                    },
                    error: function(data) {
                        alert("Uh oh, error making procedure with " + proc + "\r\n Check error log for more information");
                    }
                });
            }
        }
        else if (r.hasOwnProperty('Error'))
        {
            console.log("Error: " + r.Error);
        }
    });
}

function makeTrigger(db, trigger, id) 
{
    uri = `?setup::step/2/action/checkTrigger/${db}/${trigger}`
    $.ajax({
        url: uri,
        method: 'POST',
        data: {},
        dataType: 'JSON',
        async: false,
        error: function(data) {
            alert("Uh oh, error checking trigger with " + trigger + "\r\n Check error log for more information");
        }
    }).then(function (r) {
        if (r.hasOwnProperty('Success'))
        {
            if (r.Success == "Exist")
            {
                td = `#dbt${id}`;
                $(td).css('background-color', '#44dd44');
                $(td).text(`${trigger} Already Exists`);
            }
            else
            {
                uri = `?setup::step/2/action/makeTable/${db}/${trigger}`;
                $.ajax({
                    url: uri,
                    method: 'POST',
                    data: {},
                    dataType: 'JSON',
                    async: false,
                    success: function(r) {
                        td = `#dbt${id}`;
                        if (r.hasOwnProperty('Success'))
                        {
                            $(td).text(r.Success);
                            $(td).css('background-color', '#44dd44');
                        }
                        else if (r.hasOwnProperty('Error'))
                        {
                            $(td).text(r.Error);
                            $(td).css('background-color', '#dd4444');
                        }
                    },
                    error: function(data) {
                        alert("Uh oh, error making view with " + trigger + "\r\n Check error log for more information");
                    }
                });
            }
        }
        else if (r.hasOwnProperty('Error'))
        {
            console.log("Error: " + r.Error);
        }
    });
}

function query(uri, task, id) 
{
    $.ajax({
        url: uri,
        method: 'POST',
        data: {},
        dataType: 'JSON',
        async: false,
        success: function(r) {
            td = `#dbt${id}`;
            if (r.hasOwnProperty('Success'))
            {
                $(td).text(r.Success);
                $(td).css('background-color', '#44dd44');
            }
            else if (r.hasOwnProperty('Error'))
            {
                $(td).text(r.Error);
                $(td).css('background-color', '#dd4444');
            }
        },
        error: function(data) {
            alert("Uh oh, error with " + task + "\r\n Check error log for more information");
        }
    });
}

function stop(e)
{
    e.preventDefault();
}