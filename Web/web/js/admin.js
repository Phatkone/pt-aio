$(document).ready(function() {
    $('input[name="gift"]').on('click', function(){
        var val = $(this).val();
        if (val=="gold")
        {
            $('input[name="itCode"]').val("GG101");
            $('input[name="itCode"]').attr('readonly', true);
            $('input[name="itCode"]').removeAttr('required');
			$('#gift').fadeOut(300);
			$('#gold').delay(300).fadeIn(300);		

        }
        else if (val=="exp")
        {
            $('input[name="itCode"]').val("GG102");
            $('input[name="itCode"]').attr('readonly', true);
            $('input[name="itCode"]').removeAttr('required'); 
			$('#gift').fadeOut(300);
			$('#gold').delay(300).fadeIn(300);	
        }
        else if (val == "item")
        {
            $('#gift').show();
            $('#gold').hide();
			$('input[name="itCode"]').removeAttr('readonly');
			$('#gold').fadeOut(300);
			$('#gift').delay(300).fadeIn(300);
        }
    });
});