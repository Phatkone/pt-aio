$(document).ready(function () {
    $('#login').click(function() {
        $('#loginbgdiv').show();
    });
    $(document).on('mousedown', function(e) {
        if (e.target.id == 'loginbgdiv' && $('#loginbgdiv').is(':visible'))
        {
            $('#loginbgdiv').hide();
        }
    }); 
    $('select[name="theme"]').change(function() {
        var theme = $(this)[0].value;
        $('body').attr('class',theme);
    });
    $('#registerForm').submit(function(e) {
        var p = $('input[name="regPass"]').val();
        var p2 = $('input[name="regPassC"]').val();
        if (p2 !== p)
        {
            alert('Passwords do not match :/');
            e.preventDefault();
        }
        else
        {
            $('#regsubmit').attr('disabled',true);
        }
    });
    $('input[name="regUser"]').change(function() {
        $.ajax({
            url: '?do::checkaccount',
            method: 'POST',
            data: {'username':$('input[name="regUser"]').val()},
            dataType: 'JSON',
            error: function(data) {
                console.log(data);
            },
            success: function(data) {
                if (data['available'] == false) {
                    $('#registerMessage')[0].innerHTML = "<font color='#FF5050'>Account name is already in use</font>";
                    $('#regsubmit').attr('disabed',true);
                }
                else
                {
                    $('#registerMessage')[0].innerHTML = "<font color='#50FF50'>Account name is available</font>";
                    $('#regsubmit').removeAttr('disabed');
                }
            }
        })
    })
});
document.addEventListener('contextmenu', event => event.preventDefault());
/*css({
        top: event.pageY + "px",
        left: event.pageX + "px"
    });*/