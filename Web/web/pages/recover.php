<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $configs;
$err = "";
$recoverForm = "<div class='body'><h2>Account Recovery</h2>
{$err}
<p><form method='POST'><table>
<tr><td>Account Name:</td><td><input name='recoverAccount' type='text' required /></td></tr>
<tr><td>Email Address:</td><td><input name='recoverEmail' type='email' required /></td></tr>
<tr><td colspan=2><input type='Submit' value='Recover' /></td></tr>
</table></form></p></div>";

if (!isset($_POST['recoverAccount']) && !isset($_POST['recoverEmail']))
{
    echo $recoverForm;
}
elseif (!isset($_POST['recoverAccount']) || !isset($_POST['recoverEmail']))
{
    $err = "<font color='red'>Please fill in both fields...</font>";
    echo $recoverForm;
}
else
{
    // Account recovery section here 
    // generate token, create entry, send email. 
}
?>