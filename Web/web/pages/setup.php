<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $admin, $configs, $verified, $configfile;
if (!$admin || !$verified)
    pages::render('errorpage');
elseif (!empty($_POST))
{
    $dbHost = (isset($_POST['dbHost'])) ? $_POST['dbHost'] : false;
    $dbUser = (isset($_POST['dbUser'])) ? $_POST['dbUser'] : false;
    $dbPass = (isset($_POST['dbPass'])) ? $_POST['dbPass'] : false;
    $serverDir = (isset($_POST['serverDirectory'])) ? $_POST['serverDirectory'] : false;
    $serverAddress = (isset($_POST['serverAddress'])) ? $_POST['serverAddress'] : "127.0.0.1";
    $serverPort = (isset($_POST['serverPort'])) ? $_POST['serverPort'] : 10007;
    $timezone = (isset($_POST['timezone'])) ? $_POST['timezone'] : false;
    $theme = (isset($_POST['theme'])) ? $_POST['theme'] : false;
    $verification = (isset($_POST['verification'])) ? $_POST['verification'] : false;
    $email = (isset($_POST['email'])) ? $_POST['email'] : false;
    $smtpServer = (isset($_POST['smtpServer'])) ? $_POST['smtpServer'] : false;
    $smtpPort = (isset($_POST['smtpPort'])) ? $_POST['smtpPort'] : false;
    $smtpPass = (isset($_POST['smtpPass'])) ? $_POST['smtpPass'] : false;
    $sender = (isset($_POST['sender'])) ? $_POST['sender'] : false;
    $url = (isset($_POST['url'])) ? $_POST['url'] : false;
    $configtext = "[Database]
dbHost = {$dbHost}
dbUser = {$dbUser}
dbPass = {$dbPass}
[Server]
serverDirectory = {$serverDir}
serverAddress = {$serverAddress}
serverPort = {$serverPort}
timezone = {$timezone}
theme = {$theme}
[Email]
verification = {$verification}
email = {$email}
smtpServer = {$smtpServer}
smtpPort = {$smtpPort}
smtpPass = {$smtpPass}
sender = {$sender}
url = {$url}";
    file_put_contents($configfile, $configtext);
    echo "<script type='text/javascript'>document.location.href = '/?admin::setup';</script>";
}
else
{
    $checked = ($configs['verification']) ? 'checked' : "";
    
    echo "<div class='body'>
        <h2>Settings</h2>";
    echo ($configs['verification']) ? "Ensure PHP is configured for a proper openssl.cafile in php.ini, Failure to do so may cause emails to not work." : "";
    echo "<form method='POST'>
        <table>
            <tr><td colspan=2>Database</td></tr>
            <tr><td>Database Host</td><td><input name='dbHost' type='text' value='{$configs['dbHost']}' /></td></tr>
            <tr><td>Database User</td><td><input name='dbUser' type='text' value='{$configs['dbUser']}' /></td></tr>
            <tr><td>Database Password</td><td><input name='dbPass' type='password' value='' /></td></tr>
            <tr><td>Server Directory</td><td><input name='serverDirectory' type='text' value='{$configs['serverDirectory']}' /></td></tr>
            <tr><td>Server Address</td><td><input name='serverAddress' type='text' value='{$configs['serverAddress']}' /></td></tr>
            <tr><td>Server Port</td><td><input name='serverPort' type='text' value='{$configs['serverPort']}' /></td></tr>
            <tr><td colspan=2>Website</td></tr>
            <tr><td>Theme</td><td><select name='theme' value='{$configs['theme']}'>";
            echo ($configs['theme'] == 'light') ? "<option value='light' selected>Light</option>" : "<option value='light'>Light</option>";
            echo ($configs['theme'] == 'dark') ? "<option value='dark' selected>Dark</option>" : "<option value='dark'>Dark</option>";
            echo ($configs['theme'] == 'contrast') ? "<option value='contrast' selected>Contrast</option>" : "<option value='contrast'>Contrast</option>";
            echo "</select</td></tr>
            <tr><td>Timezone</td><td><select name='timezone' value='{$configs['timezone']}'>";
            foreach (timezone_identifiers_list() as $tz)
            {
                if ($tz == $configs['timezone'])
                    echo "<option value='{$tz}' selected>$tz</option>";
                else
                    echo "<option value='{$tz}'>$tz</option>";
            }
            echo "</select</td></tr>
            <tr><td>Email Account Verification</td><td><input name='verification' type='checkbox' $checked /></td></tr>
            <tr><td>Email Account</td><td><input name='email' type='text' value='{$configs['email']}' /></td></tr>
            <tr><td>SMTP Password</td><td><input name='smtpPass' type='password' /></td></tr>
            <tr><td>SMTP Server</td><td><input name='smtpServer' type='text' value='{$configs['smtpServer']}' /></td></tr>
            <tr><td>SMTP Port</td><td><input name='smtpPort' type='text' value='{$configs['smtpPort']}' /></td></tr>
            <tr><td>SMTP Sender</td><td><input name='sender' type='text' value='{$configs['sender']}' /></td></tr>
            <tr><td>Site URL</td><td><input name='url' type='text' value='{$configs['url']}' /></td></tr>
            <tr><td colspan=2><input type='submit' value='Update' /></td></tr>    
        </table>
        </form>
    </div>";
}
?>