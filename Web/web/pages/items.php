<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $configs, $verified, $sub, $admin;
$item = (isset($sub[1])) ? $sub[1] : null;
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass'], "ClanDB");
switch ($item)
{
	case "armors": $i = "BETWEEN 'DA101' AND 'DA131' OR code BETWEEN 'DA301' AND  'DA304'"; break;
	case "amulets": $i = "BETWEEN 'OA102' AND 'OA132'"; break;
	case "armlets": $i = "BETWEEN 'OA201' AND 'OA232'"; break;
	case "axes": $i = "BETWEEN 'WA101' AND 'WA132'"; break;
	case "boots": $i = "BETWEEN 'DB101' AND 'DB200'"; break;
	case "bows": $i = "BETWEEN 'WS101' AND 'WS132'"; break;
	case "claws": $i = "BETWEEN 'WC101' AND 'WC132'"; break;
	case "daggers": $i = "BETWEEN 'WD101' AND 'WD132'"; break;
	case "gauntlets": $i = "BETWEEN 'DG101' AND 'DG132'"; break;
	case "hammers": $i = "BETWEEN 'WH101' AND 'WH132'"; break;
	case "javelins": $i = "BETWEEN 'WT101' AND 'WT132'"; break;
	case "orbs": $i = "BETWEEN 'OM101' AND 'OM132'"; break;
	case "phantoms": $i = "BETWEEN 'WN101' AND 'WN132'"; break;
	case "rings": $i = "BETWEEN 'OR102' AND 'OR132'"; break;
	case "robes": $i = "BETWEEN 'DA201' AND 'DA231' OR code BETWEEN 'DA401' AND 'DA404'"; break;
	case "scythes": $i = "BETWEEN 'WP101' AND 'WP132'"; break;
	case "sheltoms": $i = "BETWEEN 'OS102' AND 'OS132'"; break;
	case "shields": $i = "BETWEEN 'DS101' AND 'DS132'"; break;
	case "swords": $i = "BETWEEN 'WS201' AND 'WS234'"; break;
	case "vambraces": $i = "BETWEEN 'WV101' AND 'WV132'"; break;
	case "wands": $i = "BETWEEN 'WM101' AND 'WM132'"; break;
	case "special": $i = "BETWEEN 'BI101' AND 'BI900'"; break;
	case "all": $i = "IS NOT NULL"; break;
	default: $i = "BETWEEN 'OA102' AND 'OA132'"; break;
}
if ($db->Connect() === true) 
{
	$results = $db->query("SELECT DISTINCT * FROM GameDB.dbo.Itemdetails WHERE code $i AND name NOT LIKE '%H)' ORDER BY code ASC");
	if (empty($results)) 
	{
		echo "<div class='message'><br /><h2>No Results Found!</h2></div>";
	}
	else 
	{
		$row_dark = 'style=\'background-color: var(--trd)\'';
		$row_light = 'style=\'background-color: var(--trl)\'';
		$stats = array(
		'Weight' => 'weight','Integrity' => 'integrity','Price' => 'price','Organic' => 'organic','Fire' => 'fire','Lightning' => 'lightning','Fire' =>'fire',
		'Front' => 'frost','Undead' => 'undead','Attack' => 'attack','Attack Range' => 'attack_range','Attack Rating' => 'attack_rating','Accuracy' => 'accuracy',
		'Critical' => 'critical','Absorb' => 'absorb','Defence' => 'defence','Block' => 'block','Speed' => 'speed','Pot Count' => 'potion_count','HP Regen' => 'hp_regen',
		'STM Regen' => 'stm_regen','MP Regen' => 'mp_regen','HP Add' => 'hp_add','STM Add' => 'stm_add','MP Add' =>'mp_add','APT' => 'magic_apt',
		'Level' => 'level','Strength' => 'strength','Spirit' => 'spirit','Talent' => 'talent','Agility' => 'agility','Health' => 'health',
		'Primary Spec' => 'spec_primary_class','Spec' => 'spec_class','Set HP Regen' => 'special_hp_regen','Set STM Regen' => 'special_stm_regen',
		'Set MP Regen' => 'special_mp_regen','Spec Atk' => 'spec_attack','Spec Range' => 'spec_attack_range','Spec Rating' => 'spec_attack_rating',
		'Spec Accuracy' => 'spec_accuracy','Spec Critical' => 'spec_critical','Spec Abs' => 'spec_absorb','Spec Def' => 'spec_defence',
		'Spec Block' => 'spec_block','Spec Speed' => 'spec_speed','Spec HP Add' => 'spec_hp_add','Spec STM Add' => 'spec_stm_add','Spec MP Add' => 'spec_mp_add',
		'Spec HP Regen' => 'spec_hp_regen','Spec STM Regen' => 'spec_stm_regen','Spec MP Regen' => 'spec_mp_regen');

		echo "<div class='body'><h2 class='divTitle'>".ucwords($item)."</h2><h4>
		Click On An Item Name To Search For It In Monster Drops!</h4>";
		$count = 0;
		echo "<table class='itemsTable'>";
		foreach ($results as $row)
		{
			$name = trim($row['name']);  $code = $row['code'];
			if (substr($name, -6, 4) != '(5H)' && !stripos($name, 'Day') !== false)
			{
				$rc = $row_light;
				if ($count == 0)
					echo "<tr>";
				$urlname = urlencode($name);
				echo "<td><table class='items' style='border:1px solid black; border-collapse:collapse'>";
				echo "<tr><td colspan='100%' style='background-color:black;color:white;text-align:center; height:88px;'><img src='/web/images/items/it$code.bmp' style='float:left' /><p style='padding-top:22px;height:100%'><a href='?info::dropsearch/$urlname'>$name</a> </p></td></tr>";
				foreach ($stats as $k => $v)
				{
					if (isset($row["$v"]) && $row["$v"] != "" && $row["$v"] != null)
					{
						echo "<tr $rc'><td>{$k}</td><td>{$row[$v]}</td></tr>";
						$rc = ($rc == $row_light) ? $row_dark : $row_light;
					}
				}
				echo ($admin) ? "<tr $rc;border-bottom: 1px solid black'><td>Item Code:</td><td> ".strtoupper($code)." </td></tr>" : "";
				if ($count < 3)
					echo "</table></td>";
				else
					echo "</table></td></tr>";
				$count++;
				if ($count >= 4)
					$count = 0;
			}
		}
		if ($count > 0)
		{
			$remainder = 4 - $count;
			echo "<td colspan={$remainder}></td></tr>";
		}
		echo "</table></div>";
	}	
}
else
{
    ?>
    <div class='body'>
        <h2>Uh-Oh!</h2>
        <p>Error connecting to database</p>
    </div>
    <?php
}
?>