<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $admin, $configs, $verified;
if (!$admin || !$verified)
    pages::render('errorpage');
else
{
    $block = (isset($_POST['Block'])) ? filterQuery($_POST['Block']) : "0";
    $blockUserid = (isset($_POST['blockUserid'])) ? filterQuery($_POST['blockUserid']) : "";
    $db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass'], "AccountDB");
    if ($db->Connect() === true)
    {
        if ($block != "" && $blockUserid != "")
        {
            $dbtable = (ctype_digit($blockUserid[0])) ? 'AGameUser' : $blockUserid[0]."GameUser";
            $query = "UPDATE accountdb.dbo.AllGameUser SET BlockChk='$block' WHERE userid='$blockUserid';
                    UPDATE accountdb.dbo.$dbtable SET BlockChk='$block' WHERE userid='$blockUserid';";
            if ($db->nonQuery($query))
            {
                switch ($block)
                {
                    case '1': $block = 'Blocked'; $bgc = '#f66'; break;
                    case '0': $block = 'Unblocked'; $bgc = '#6f6'; break;
                }
                echo "<div class='body'><h3>Success!</h3>
                <p>".ucwords($blockUserid)." has been $block!</p></div>";
                unset($_POST);
                echo '<meta http-equiv="Refresh" content="2;">';
            }
            else
            {
                echo "<div class='body'><h3>Uh Oh </h3> <p> An error occured. </p></div>";
            }
        }
        else 
        {
            echo "<div class='body'><h3> Account Block Tool </h3>";
            $query = "SELECT userid,BlockChk FROM Accountdb.dbo.AllGameUser ORDER BY BlockChk DESC, userid";
            $result = $db->query($query);
            echo "<center><form action='".htmlspecialchars($_SERVER['REQUEST_URI'])."'method='POST'>";
            echo "<select name='blockUserid' style='width:auto;' >";
            foreach ($result as $row)
            {
                $bUserid = $row['userid'];
                $blocked = ($row['BlockChk'] == '1') ? "Blocked" : "Not Blocked";
                $bgc = ($row['BlockChk'] == '1') ? '#f66' : '#6f6';
                echo "<option value='$bUserid' style='background-color:$bgc'>$bUserid, $blocked</option>";
            }
            echo "</select><br />
            <input type='radio' name='Block' value='1' style='width:12px'>Block
            <input type='radio' name='Block' value='0' checked style='width:12px'>Unblock
            <br /><input type='submit' value='Submit'></form></div>";
        }
    }
    else
    {
        echo "<div class='body'><h3> Unable To Contact Database </h3></div>";
    }
}
?>