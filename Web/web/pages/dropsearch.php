<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $configs, $sub, $_POST;
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
if ($db->Connect() === true)
{
    if (isset($sub[1]) || isset($_POST['itemname']))
    {
        if (!isset($sub[1]))
            $item = $_POST['itemname'];
        else
            $item = str_replace("'","''",urldecode($sub[1]));
        
        $code = $db->query("SELECT code FROM [GameDB].[dbo].[ItemDetails] WHERE name LIKE '%{$item}%'");
        if (empty($code))
        {
            echo "<div class='body'><h2>Uh-Oh!</h2><p>Seems we can't find an item by that name :/</p></div>";    
        }
        else
        {
            $code = $code[0]['code'];
            $name = (isset($sub[1])) ? urldecode($sub[1]) : $_POST['itemname'];
            $results = $db->query("SELECT name FROM [GameDB].[dbo].[MonsterDetails] WHERE drop1 LIKE '%{$code}%' OR drop2 LIKE '%{$code}%' OR drop3 LIKE '%{$code}%' OR drop4 LIKE '%{$code}%' OR drop5 LIKE '%{$code}%' OR drop6 LIKE '%{$code}%'");
            echo "<div class='body'><h2>{$name}</h2><table class='grid'>";
            $count = 0;
            foreach ($results as $monster)
            {
                if ($count == 0)
                    echo "<tr>";
                $mob = str_replace('"',"",$monster['name']);
                echo "<td>{$mob}</td>";
                $count ++;
                if ($count == 4)
                {
                    $count = 0;
                    echo "</tr>";
                }
            }            
            if ($count)
            {
                $fill = 4-$count;
                echo "<td colspan=$fill></td>";
            }
            echo "</table></div>";
        }
    }
    else
    {
        ?>
        <div class='body'>
            <h2>Item Drop Search</h2>
            <table>
            <form method='POST'>
            <tr><td>Item Name:</td><td><input type='text' name='itemname'/></td></tr>
            <tr><td colspan=2><input type='Submit' value='Search' /></td></tr>
            </form>
            </table>
        </div>
        <?php
    }
}
else
{
    echo "<div class='body'><h2>Uh-Oh!</h2><p>It seems we're having a problem connecting to the database.</p></div>";
}
?>