<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $admin, $configs, $verified, $uname;
if (empty($_POST) && $verified && $admin)
{
?>
<div class="body">
    <h2> Item Distributor </h2>
    <form id="ItemDist" method="POST">
        <table >
        <tr><td>User Account: </td><td><input type="text" name="giftuser" required></td></tr>
        <tr><td><input type="radio" name="gift" value="gold"/>Gold</td><td><input type="radio" name="gift" value="exp"/>EXP</td><td><input type="radio" name="gift" value="item" checked/>Item</td></tr>
        <tr><td>Item to gift: </td><td><input type="text" name="itCode" required></td></tr>
        <tr id="gift"><td>Item Spec: </td><td><select name="spec">
            <option value="0">No Spec</option>
            <option value="1">Fighter</option>
            <option value="2">Mechanician</option>
            <option value="3">Archer</option>
            <option value="4">Pikeman</option>
            <option value="5">Atalanta</option>
            <option value="6">Knight</option>
            <option value="7">Magician</option>
            <option value="8">Priestess</option>
            <option value="9">Assassin</option>
            <option value="10">Shaman</option>
            <option value="11">Martial Artist</option>
        </select></td></tr>
        <tr id="gold" style="display:none"><td>Amount</td><td><input type="text" name="val"></td></tr>
        <tr><td>Message: </td><td><input type="text" name="msg" required></td></tr>
        <tr><td colspan="2" style="text-align:center; position: relative; right: -40px;"><input type="submit" value="Gift It!"></td></tr>
        </table>
        <input name="reqUser" style="display:none" value="<?=$uname?>"/>
    </form>	
</div>
<?php
}
else
{
    $giftuser = (isset($_POST['giftuser'])) ? $_POST['giftuser'] : false;
    $reqUser = (isset($_POST['reqUser'])) ? $_POST['reqUser'] : false;
    $gift = (isset($_POST['gift'])) ? $_POST['gift'] : false;
    $itCode = (isset($_POST['itCode'])) ? $_POST['itCode'] : false;
    $spec = (isset($_POST['spec'])) ? $_POST['spec'] : false;
    $msg = (isset($_POST['msg'])) ? $_POST['msg'] : false;
    $val = (isset($_POST['val'])) ? $_POST['val'] : false;
    if (!$reqUser)
    {
        echo "<div class='body'><h2>Unauthorised Access</h2><p>You are not authorised to use this tool.</p></div>";
    }
    elseif ($spec > 11 || $spec < 0)
    {
        echo "<div class='body'><h2>Uh-Oh!</h2><p>Invalid spec entered</p></div>";
    }
    elseif (!is_numeric($val))
    {
        echo "<div class='body'><h2>Uh-Oh!</h2><p>Qty must be a valid whole number.</p></div>";
    }
    else
    {
        if ($gift == "gold")
        {
            $itCode = "GG101";
            $spec = $val;
        }
        elseif ($gift == "exp")
        {
            $itCode = "GG102";
            $spec = $val;
        }
        $file = $configs['serverDirectory']."/PostBox/".numDir($giftuser)."/{$giftuser}.dat";
        $send = file_put_contents($file, "***   {$itCode}   {$spec} \"{$msg}\"", FILE_APPEND);
        if ($send)
            echo "<div class='body'><h2>Done</h2><p>{$giftuser} has been sent {$itCode}</p></div>";
        else
            echo "<div class='body'><h2>Uh-Oh!</h2><p>An error has occured.</p></div>";
    }
}
?>