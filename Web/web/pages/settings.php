<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $configs, $sub, $_POST, $uname;
require_once('./common/classes.php');
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
if (isset($uname) && $uname != "")
{
    if ($db->Connect() === true)
    {
        $email = $db->query("SELECT email from [LogDB].[dbo].[AccountEmail] WHERE account LIKE '{$uname}'");
        if (empty($email))
            $email = "";
        else
            $email = $email[0]['email'];
        $update = "";
        if (isset($_POST['updateEmail']))
        {
            if (empty($db->query("SELECT COUNT(*) FROM [AccountDB].[dbo].[AllGameUser] WHERE userid LIKE '{$uname}' AND passwd = '{$_POST['updateCurrentPassword']}'")))
            {
                $update = "<p style='color:red'>Incorrect Password</p>";
            }
            else
            {
                if ($_POST['updateNewPassword'] !== $_POST['updateNewPassword'])
                {
                    $update = "<p style='color:red'>New Passwords Do Not Match</p>";
                }
                else
                {
                    if ($email !== $_POST['updateEmail'])
                    {
                        if ($db->nonQuery("UPDATE [LogDB].[dbo].[AccountEmail] SET email = '{$_POST['updateEmail']}' WHERE account LIKE '{$uname}'"))
                        {
                                $update = "<p>Email Updated Successfully</p>";
                                $email = $_POST['updateEmail'];
                        }
                        else
                            $update = "<p>We encountered an error updating your email :/</p>";
                        
                    }
                    if ($_POST['updateNewPassword'] != "")
                    {
                        if ($db->nonQuery("UPDATE [AccountDB].[dbo].[AllGameUser] SET passwd = '{$_POST['updateNewPassword']}' WHERE userid LIKE '{$uname}'") && $db->nonQuery("UPDATE [AccountDB].[dbo].[{$userid[0]}GameUser] SET passwd = '{$_POST['updateNewPassword']}' WHERE userid LIKE '{$uname}'"))
                                $update = "<p>Password Updated Successfully</p>";
                        else
                            $update = "<p>We encountered an error updating your password :/</p>";
                    }
                }
            } 
        }
        ?>
        <div class='body'>
            <h2><?=$uname?> Account Settings</h2>
            <?=$update?>
            <table>
            <form method='POST'>
                <tr><td>Email:</td><td><input type='email' name='updateEmail' value='<?=$email?>' /></td></tr>
                <tr><td>Password:</td><td><input type='password' name='updateCurrentPassword' /></td></tr>
                <tr><td>New Password:</td><td><input type='password' name='updateNewPassword' /></td></tr>
                <tr><td>Confirm Password:</td><td><input type='password' name='updateNewPasswordC' /></td></tr>
                <tr><td colspan=2><input type='submit' value='Update' /></td></tr>
            </form>
            </table>
        </div>
        <?php
        $chars = $db->query("SELECT * FROM [GameDB].[dbo].[LevelList] WHERE account LIKE '{$uname}'");
        if (empty($chars))
        {
            echo "<div class='body'><h2>Oh No!</h2><p>You don't have any characters yet!</p></div>";
        }
        else
        {
            echo "<div class='body'><h2>Your Characters</h2>";
            foreach ($chars as $char)
            {
                $t11 = $char['t1_1'];$t12 = $char['t1_2'];$t13 = $char['t1_3'];$t14 = $char['t1_4'];
                $t11pc = $char['t1_1t']/100;$t12pc = $char['t1_2t']/100;$t13pc = $char['t1_3t']/100;$t14pc = $char['t1_4t']/100;
                $t21 = $char['t2_1'];$t22 = $char['t2_2'];$t23 = $char['t2_3'];$t24 = $char['t2_4'];
                $t21pc = $char['t2_1t']/100;$t22pc = $char['t2_2t']/100;$t23pc = $char['t2_3t']/100;$t24pc = $char['t2_4t']/100;
                $t31 = $char['t3_1'];$t32 = $char['t3_2'];$t33 = $char['t3_3'];$t34 = $char['t3_4'];
                $t31pc = $char['t3_1t']/100;$t32pc = $char['t3_2t']/100;$t33pc = $char['t3_3t']/100;$t34pc = $char['t3_4t']/100;
                $t41 = $char['t4_1'];$t42 = $char['t4_2'];$t43 = $char['t4_3'];$t44 = $char['t4_4'];
                $t51 = $char['t5_1'];$t52 = $char['t5_2'];$t53 = $char['t5_3'];$t54 = $char['t5_4'];
                $tier = $char['JobCode']+1;
                echo "<h3 style='padding-top: 25px; padding-bottom: 10px;'>{$char['CharName']} Level {$char['CharLevel']} {$classes[$char['CharClass']][$tier]}</h3>
                <table class='grid chartable'>
                <tr><th colspan='10'>Character Info</th></tr>
                <tr><td>Tier:</td><td>{$tier}</td><td>Exp:</td><td>{$char['Exp']}</td><td>Gold:</td><td>{$char['Gold']}</td><td>Field:</td><td>{$char['Field']}</td><td></td><td></td></tr>
                <tr><td>Strength:</td><td>{$char['Strength']}</td><td>Talent:</td><td>{$char['Talent']}</td><td>Spirit:</td><td>{$char['Spirit']}</td><td>Agility:</td><td>{$char['Agility']}</td><td>Health:</td><td>{$char['Health']}</td></tr>
                <tr><th colspan='10'>Skill Levels</th></tr>
                <tr>
                <td><img src='/web/images/skills/{$char['CharClass']}11".skillimg($t11).".bmp' /></td><td>L: $t11<div class='percentBar'><div class='percentage' style='width:{$t11pc}%;'>{$t11pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}12".skillimg($t12).".bmp' /></td><td>L: $t12<div class='percentBar'><div class='percentage' style='width:{$t12pc}%;'>{$t12pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}13".skillimg($t13).".bmp' /></td><td>L: $t13<div class='percentBar'><div class='percentage' style='width:{$t13pc}%;'>{$t13pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}14".skillimg($t14).".bmp' /></td><td>L: $t14<div class='percentBar'><div class='percentage' style='width:{$t14pc}%;'>{$t14pc}%</div></div></td>
                <td></td><td></td></tr>
                <tr>
                <td><img src='/web/images/skills/{$char['CharClass']}21".skillimg($t21).".bmp' /></td><td>L: $t21<div class='percentBar'><div class='percentage' style='width:{$t21pc}%;'>{$t21pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}22".skillimg($t22).".bmp' /></td><td>L: $t22<div class='percentBar'><div class='percentage' style='width:{$t11pc}%;'>{$t11pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}23".skillimg($t23).".bmp' /></td><td>L: $t23<div class='percentBar'><div class='percentage' style='width:{$t11pc}%;'>{$t11pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}24".skillimg($t24).".bmp' /></td><td>L: $t24<div class='percentBar'><div class='percentage' style='width:{$t24pc}%;'>{$t24pc}%</div></div></td>
                <td></td><td></td></tr>
                <tr>
                <td><img src='/web/images/skills/{$char['CharClass']}31".skillimg($t31).".bmp' /></td><td>L: $t31<div class='percentBar'><div class='percentage' style='width:{$t31pc}%;'>{$t31pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}32".skillimg($t32).".bmp' /></td><td>L: $t32<div class='percentBar'><div class='percentage' style='width:{$t11pc}%;'>{$t11pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}33".skillimg($t33).".bmp' /></td><td>L: $t33<div class='percentBar'><div class='percentage' style='width:{$t11pc}%;'>{$t11pc}%</div></div></td>
                <td><img src='/web/images/skills/{$char['CharClass']}34".skillimg($t34).".bmp' /></td><td>L: $t34<div class='percentBar'><div class='percentage' style='width:{$t34pc}%;'>{$t34pc}%</div></div></td>
                <td></td><td></td></tr>
                <tr>
                <td><img src='/web/images/skills/{$char['CharClass']}41".skillimg($t41).".bmp' /></td><td>L: $t41</td>
                <td><img src='/web/images/skills/{$char['CharClass']}42".skillimg($t42).".bmp' /></td><td>L: $t42</td>
                <td><img src='/web/images/skills/{$char['CharClass']}43".skillimg($t43).".bmp' /></td><td>L: $t43</td>
                <td><img src='/web/images/skills/{$char['CharClass']}44".skillimg($t44).".bmp' /></td><td>L: $t44</td>
                <td></td><td></td></tr>
                <tr>
                <td><img src='/web/images/skills/{$char['CharClass']}51".skillimg($t51).".bmp' /></td><td>L: $t51</td>
                <td><img src='/web/images/skills/{$char['CharClass']}52".skillimg($t52).".bmp' /></td><td>L: $t52</td>
                <td><img src='/web/images/skills/{$char['CharClass']}53".skillimg($t53).".bmp' /></td><td>L: $t53</td>
                <td><img src='/web/images/skills/{$char['CharClass']}54".skillimg($t54).".bmp' /></td><td>L: $t54</td>
                <td></td><td></td></tr>
                </table>";
            }
            echo "</div>";
        }
    }
    else
    {
        echo "<div class='body'><h2>Uh-Oh!</h2><p>It seems we're having a problem connecting to the database.</p></div>";
    }
}

function skillimg($lvl)
{
    if (!$lvl)
        return "_gray";
}

/*
// Used for copying the skill images into grayscale variant
foreach (range(111,1154) as $int)
{
    if (file_exists("./web/images/skills/$int.bmp"))
    {
        skill_image($int);
    }
}

function skill_image($image)
{
    if (file_exists("./web/images/skills/{$image}.bmp"))
    {  
        $img = imagecreatefrombmp("./web/images/skills/{$image}.bmp");
        if ($img && imagefilter($img, IMG_FILTER_GRAYSCALE))
            imagebmp($img, "./web/images/skills/{$image}_gray.bmp");
        imagedestroy($img);
    }
}
*/
?>