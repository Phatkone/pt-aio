<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $configs;
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
if ($db->Connect() === true)
{
    $clans = $db->query("SELECT a.ClanName, a.Note, a.miconcnt, a.memcnt, a.ClanZang FROM [ClanDB].[dbo].[CL] a ORDER BY a.memcnt DESC");
    if (empty($clans))
    {
        echo "<div class='body'><h2>Where are they?</h2><p>It seems there are no clans yet!</p></div>";
    }
    else
    {
        echo "<div class='body'><h2>Clans</h2><table class='grid'>
        <tr><th>Clan Name</th><th>Clan Note</th><th>Members</th><th>Clan Leader</th></tr>";
        foreach ($clans as $clan)
        {
            echo "<tr><td><img src='/clancontent/{$clan['miconcnt']}.bmp' class='clanicon' />{$clan['ClanName']}</td><td>{$clan['Note']}</td><td>{$clan['memcnt']}</td><td>{$clan['ClanZang']}</td></tr>";
        }
        echo "</table></div>";
    }
}
else
{
    echo "<div class='body'><h2>Uh-Oh!</h2><p>It seems we're having a problem connecting to the database.</p></div>";
}
?>