<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $configs;
$regform = "<form method='POST' id='registerForm'>
<table>
<tr><td>Username:</td><td><input type='text' name='regUser' pattern='[A-Za-z]{1}[A-Za-z0-9]{0,15}' title='Must start with an alpha character (A-Z) and be alpha numeric only' required/></td></tr>
<tr><td>Password:</td><td><input type='password' name='regPass' pattern='[A-Za-z]{1}[A-Za-z0-9!@#$%^&amp;*\(\)]{0,15}' title='A-Z, 0-9 and characters !@#$%^&*()' required/></td></tr>
<tr><td>Confirm Password:</td><td><input type='password' name='regPassC' pattern='[A-Za-z]{1}[A-Za-z0-9!@#$%^&amp;*\(\)]{0,15}' required/></td></tr>
<tr><td>Email Address:</td><td><input type='email' name='regEmail' required/></td></tr>
<tr><td colspan=2><input type='submit' id='regsubmit' value='Register' /></td></tr>
<tr><td colspan=2 id='registerMessage'></td></tr>
</table>
</form>";
if (!empty($_POST))
{
    $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
    $regUser = (isset($_POST['regUser'])) ? filterQuery($_POST['regUser']) : false;
    $regPass = (isset($_POST['regPass'])) ? filterQuery($_POST['regPass']) : false;
    $regPassC = (isset($_POST['regPassC'])) ? filterQuery($_POST['regPassC']) : false;
    $regEmail = (isset($_POST['regEmail'])) ? filterQuery($_POST['regEmail']) : false;
    if (!$regUser || !$regPass || !$regPassC || !$regEmail)
        die("<div class='body'><h2>Register</h2><font color='red'>Please fill in all fields</font>{$regform}</div>");
    if ($regPass !== $regPassC)
        die("<div class='body'><font color='red'>Passwords do not match</font>{$regform}</div>");
    if (!valid_account($regUser))
        die("<div class='body'><h2>Register</h2><font color='red'>Invalid username. Username must start with a character (A-Z) and be alpha numeric only.</font>{$regform}</div>");
    if (!valid_password($regPass))
        die("<div class='body'><h2>Register</h2><font color='red'>Invalid password. Password must start with a character (A-Z).</font>{$regform}</div>");
    if (accCreate($regUser, $regPass, $regEmail, $_SERVER['REMOTE_ADDR'],$configs))
    {
        if ($configs['verification'] == true)
        {
            ?>
            <div class='body'>
                <h2>Success!</h2>
                <p>Your account has successfully been created!. <br />
                You will be sent a verification email to confirm your account <br />
                You will have 24 hours to verify your account.</p>
            </div>
            <?php
        }
        else
        {
            ?>
            <div class='body'>
                <h2>Success!</h2>
                <p>Your account has successfully been created!</p>
            </div>
            <?php
        }
    }
    else
    {
        ?>
        <div class='body'>
            <h2>Oops!</h2>
            <p>An error occurred. <br /> Please try again or contact an administrator for assistance. <?=$regform?></p>
        </div>
        <?php
    }
}
else
{
    ?>
    <div class='body'>
        <h2>Register</h2>
        <?=$regform?>
    </div>
    <?php
}