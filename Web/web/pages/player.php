<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $configs, $classes;
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
if ($db->Connect() === true)
{
    $players = $db->query("SELECT TOP(50) CharName, CharClass, CharLevel FROM [GameDB].[dbo].[Charlist] ORDER BY CharLevel DESC");
    if (empty($players))
    {
        echo "<div class='body'><h2>Where are they?</h2><p>It seems there are no characters yet!</p></div>";
    }
    else
    {
        echo "<div class='body'><h2>Top 50 Players</h2><table class='grid'>
        <tr><th>Class</th><th>Name</th><th>Level</th></tr>";
        foreach ($players as $player)
        {
            echo "<tr><td><img src='/web/images/{$player['CharClass']}.bmp' class='clanicon' />{$classes[$player['CharClass']]}</td><td>{$player['CharName']}</td><td>{$player['CharLevel']}</td></tr>";
        }
        echo "</table></div>";
    }
}
else
{
    echo "<div class='body'><h2>Uh-Oh!</h2><p>It seems we're having a problem connecting to the database.</p></div>";
}
?>