<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $configs, $sub;
$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
if ($db->Connect() === true)
{
    if ($sub[1] == 'clan')
    {
        $clans = $db->query("SELECT a.ClanName, a.miconcnt, a.CPoint FROM [ClanDB].[dbo].[CL] a WHERE a.CPoint > 0 ORDER BY a.CPoint DESC");
        if (empty($clans))
        {
            echo "<div class='body'><h2>Where are they?</h2><p>It seems there are no clan scores yet!</p></div>";
        }
        else
        {
            echo "<div class='body'><h2>Clans</h2><table class='grid'>
            <tr><th>Clan</th><th>Score</th></tr>";
            foreach ($clans as $clan)
            {
                echo "<tr><td><img src='/clancontent/{$clan['miconcnt']}.bmp' class='clanicon' />{$clan['ClanName']}</td><td>{$clan['CPoint']}</td></tr>";
            }
            echo "</table></div>";
        }
    }
    elseif ($sub[1] == 'player')
    {
        $YM = date("ym");
        $scores = $db->query("SELECT a.CharName, a.CharType, a.Point, a.KillCount FROM [SodDB].[dbo].[SoD2Record{$YM}] a WHERE a.Point > 0 ORDER BY a.Point DESC");
        if (empty($scores))
        {
            echo "<div class='body'><h2>Where are they?</h2><p>It seems there are no scores yet!</p></div>";
        }
        else
        {
            echo "<div class='body'><h2>Clans</h2><table class='grid'>
            <tr><th>Player</th><th>Score</th><th>Kill Count</th></tr>";
            foreach ($scores as $score)
            {
                echo "<tr><td><img src='/web/images/{$score['CharType']}.bmp' class='clanicon' />{$score['CharName']}</td><td>{$score['Point']}</td><td>{$score['KillCount']}</td></tr>";
            }
            echo "</table></div>";
        }
    }
}
else
{
    echo "<div class='body'><h2>Uh-Oh!</h2><p>It seems we're having a problem connecting to the database.</p></div>";
}
?>