<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
global $admin, $configs, $verified;
if (!$admin || !$verified)
    pages::render('errorpage');
else
{
    $db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass'], "ClanDB");
    if ($db->Connect() !== true)
    {
    ?>
        <div class='body'>
            <h2>Uh-Oh!</h2>
            <p>Error connecting to database :/</p>
        </div>
    <?php
    }
    else
    {
        $count = $db->query("SELECT DISTINCT COUNT(*) as entries FROM [ClanDB].[dbo].[CT]")[0]['entries'];
    ?>
    <div class='body'>
    <h2><?=$count?> Current Player(s)</h2>
    <table class='grid'>
        <tr><th>Account</th><th>Name</th><th>Class</th><th>Level</th><th>Clan</th><th>Field</th><th>IP Address</th></tr>
    <?php
        $players = $db->query(
            "SELECT a.UserID,
                a.ChName, 
                (SELECT namedClass 
                    FROM [GameDB].[dbo].[Classes] 
                    WHERE intClass = b.CharClass) as ChType, 
                b.CharLevel, 
                (SELECT ClanName 
                    FROM CL 
                    WHERE MIconCnt = (SELECT MIconCnt 
                                            FROM UL 
                                            WHERE ChName=a.ChName)) AS ClanName, 
                b.Field,
                a.IP
            FROM [CT] a 
            JOIN [GameDB].[dbo].[CharList] b 
                ON (a.ChName = b.CharName) 
            WHERE a.UserID IS NOT NULL
            ORDER BY UserID DESC,
                     ChName DESC");
        foreach ($players as $player)
        {
            if ($player['ClanName'] == null)
                $player['ClanName'] = "None";
            echo "<tr><td>{$player['UserID']}</td><td>{$player['ChName']}</td><td>{$player['ChType']}</td><td>{$player['CharLevel']}</td><td>{$player['ClanName']}</td><td>{$player['Field']}</td><td>{$player['IP']}</td></tr>";
        }
    ?>
        </table>
        </div>
    <?php
    }
}
?>