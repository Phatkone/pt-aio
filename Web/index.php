<?php
define('PT',1);
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
require_once('common/common.php');
require_once('./common/pages.class.php');
if ($page == "setup")
    require_once('setup/setup.php');
elseif ($firstrun)
{
    header("Server: PT Server By Ashikabi");
    header('Location: ?setup::step/1');
}
elseif ($page == 'do')
{
    pages::render($sub[0],'php');
}
else
{
    require_once('web/index.php');
    date_default_timezone_set($configs['timezone']);
}
?>