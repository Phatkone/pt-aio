<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$dbHost = (isset($_POST['dbHost'])) ? $_POST['dbHost'] : false;
$dbSA = (isset($_POST['dbSA'])) ? $_POST['dbSA'] : false;
$dbSAP = (isset($_POST['dbSAP'])) ? $_POST['dbSAP'] : false;
$dbUser = (isset($_POST['dbUser'])) ? $_POST['dbUser'] : false;
$dbPass = (isset($_POST['dbPass'])) ? $_POST['dbPass'] : false;
$serverDir = (isset($_POST['serverDir'])) ? $_POST['serverDir'] : false;
$serverAddress = (isset($_POST['serverAddress'])) ? $_POST['serverAddress'] : "127.0.0.1";
$serverPort = (isset($_POST['serverPort'])) ? $_POST['serverPort'] : 10007;
$timezone = (isset($_POST['timezone'])) ? $_POST['timezone'] : false;
$theme = (isset($_POST['theme'])) ? $_POST['theme'] : false;
$verification = (isset($_POST['verification'])) ? $_POST['verification'] : false;
$email = (isset($_POST['email'])) ? $_POST['email'] : false;
$smtpServer = (isset($_POST['smtpServer'])) ? $_POST['smtpServer'] : false;
$smtpPort = (isset($_POST['smtpPort'])) ? $_POST['smtpPort'] : false;
$smtpPass = (isset($_POST['smtpPass'])) ? $_POST['smtpPass'] : false;
$sender = (isset($_POST['sender'])) ? $_POST['sender'] : false;
$url = (isset($_POST['url'])) ? $_POST['url'] : false;
@session_start();
$_SESSION['dbHost'] = $dbHost;
$_SESSION['dbSA'] = $dbSA;
$_SESSION['dbSAP'] = $dbSAP;
$_SESSION['dbUser'] = $dbUser;
$_SESSION['dbPass'] = $dbPass;
if (file_exists('setup/temp.ini'))
{
    $stngs = parse_ini_file('setup/temp.ini');
    $dbHost = $stngs['dbHost'];
    $dbSA = $stngs['dbSA'];
    $dbSAP = $stngs['dbSAP'];
    $dbUser = $stngs['dbUser'];
    $dbPass = $stngs['dbPass'];
}

$configtext = "[Database]
dbHost = {$dbHost}
dbUser = {$dbUser}
dbPass = {$dbPass}
[Server]
serverDirectory = {$serverDir}
serverAddress = {$serverAddress}
serverPort = {$serverPort}
timezone = {$timezone}
theme = {$theme}
[Email]
verification = {$verification}
email = {$email}
smtpServer = {$smtpServer}
smtpPort = {$smtpPort}
smtpPass = {$smtpPass}
sender = {$sender}
url = {$url}";
file_put_contents($configfile, $configtext);

$clanscript = "<?php
/*\"LOCAL\SQLEXPRESS\";  \"123.1.2.3,1433\";  Server IP Address OR instance name  */
\$server = '{$dbHost}'; //Sql Server
\$UID = '{$dbUser}';	// SQL Username
\$PWD = '{$dbPass}';	//SQL Password
date_default_timezone_set('{$timezone}');
require_once('../ServerMain/common.php');
?>";
file_put_contents('ServerMain/settings.php', $clanscript);
?>
<h2>Step 4</h2>
<p>Populating Databases (this may take a while)</p>
<table class='setuptable'><tr><th>Task</th><th>Result</th></tr>
<tr><td>Populate Fields Table</td><td id='dbt1'>Pending</td></tr>
<tr><td>Populate Item Details</td><td id='dbt2'>Pending</td></tr>
<tr><td>Populate Monster Details</td><td id='dbt3'>Pending</td></tr>

<tr><td colspan=2><input type='Submit' value='Back' onClick='window.location.href = "?setup::step/3"' />&nbsp<input type='Submit' value='Continue' onClick='window.location.href = "?setup::step/5";' /></td></tr>
</table>
<script type='text/javascript'>
$(document).ready(function() {
    setTimeout(() => {
        query('?setup::step/4-2/action/fields', 'populating fields',1);
        query('?setup::step/4-2/action/items', "populating items",2);
        query('?setup::step/4-2/action/monsters', "populating monsters",3);
    }, 500);
});</script>