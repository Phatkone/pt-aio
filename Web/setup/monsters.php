<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
ini_set("max_execution_time", 0);
require_once("common\\monster.class.php");
$dir = $configs['serverDirectory'].'\\GameServer\\Monster\\';
$items = scandir($dir);
$conn = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass'], 'GameDB');
$errors = false;
if ($conn->connect() !== true)
    die(json_encode(["Error" => "Error connecting to database"]));
if (is_dir($dir) && $files = scandir($dir))
{
    foreach ($files as $file)
    {
        if (is_file($dir.$file))
        {
            $mon = new monster($dir.$file);
            if ($mon->ename == null || $mon->ename == "")
                continue;
            $name = str_replace("'","''",$mon->ename);
            $kname = str_replace("'","''",$mon->kname);
            $query = "INSERT INTO [GameDB].[dbo].[Monster]
            (ename, kname, boss, modelsize, attribute, shapefile, screenadjust, level, timelimit, grouping, intellect, nature, vision,
            hp, attack, absorb, block, defence, attackspeed, accuracy, specattackrate, size, attackrange, organic, lightning, ice, fire,
            poison, magic, race, movespeed, movetype, soundeffect, exp, eventitem, itemcount, namefile)
            VALUES
            ('{$name}', N'{$kname}', '{$mon->boss}', '{$mon->modelsize}', '{$mon->attribute}', '{$mon->shapefile}', '{$mon->screenadjust}',
                {$mon->level}, '{$mon->timelimit}', '{$mon->grouping}', '{$mon->intellect}', '{$mon->nature}', {$mon->vision}, {$mon->hp},
                '{$mon->attack}', '{$mon->absorb}', '{$mon->block}', {$mon->defence}, {$mon->attackspeed}, {$mon->accuracy}, '{$mon->specattackrate}',
                '{$mon->size}', '{$mon->attackrange}', '{$mon->organic}', '{$mon->lightning}', '{$mon->ice}', '{$mon->fire}', '{$mon->poison}',
                '{$mon->magic}', '{$mon->race}', '{$mon->movespeed}', '{$mon->movetype}', '{$mon->soundeffect}', {$mon->exp}, '{$mon->eventitem}',
                {$mon->itemcount}, '{$mon->namefile}'
            )";
            $r = ($query);
            if ($conn->nonQuery($query) === false)
            {
                error_log("Error inserting {$mon->ename} : {$mon->monfile} \r\n {$query}");
                $errors = true;
            }
            if (is_array($mon->drops))
            {
                foreach ($mon->drops as $arr)
                {
                    foreach ($arr as $rate => $drops)
                    {
                        $query = "INSERT INTO [GameDB].[dbo].[MonsterDrops]
                        (monster, droprate, drops)
                        VALUES 
                        ('{$name}', '{$rate}', '{$drops}')";
                        if ($conn->nonQuery($query) === false)
                        {
                            $errors = true;
                            error_log("Error inserting drops {$mon->name} : {$mon->monfile} \r\n {$rate} {$drops} \r\n {$query}");
                        }
                    }
                }
            }
            if (is_array($mon->moneydrop))
            {
                foreach ($mon->moneydrop as $rate => $amount)
                {
                    $query = "INSERT INTO [GameDB].[dbo].[MonsterDrops]
                        (monster, droprate, drops)
                        VALUES 
                        ('{$name}', '{$rate}', '{$amount}')";
                        if ($conn->nonQuery($query) === false)
                        {
                            $errors = true;
                            error_log("Error inserting money drop {$mon->ename} : {$mon->monfile} \r\n {$rate} {$amount} \r\n {$query}");
                        }
                }
            }
        }
    }
    $conn->close();
    if ($errors)
        die(json_encode(["Success" => "Monster detail population succeeded with errors"]));
    else
        die(json_encode(["Success" => "Successfully populated monster details"]));
}
else
{
    die(json_encode(["Error" => "Unable to open directory {$dir}"]));
}
?>