<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$tables['WebsiteDB']['AdminNotices'] = "
IF NOT EXISTS ( SELECT * FROM [WebsiteDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'AdminNotices' )
BEGIN
    CREATE TABLE [WebsiteDB].[dbo].[AdminNotices](
        [id] [int] NOT NULL,
        [heading] [nvarchar](100) NOT NULL,
        [message] [nvarchar](max) NOT NULL,
        [footer] [nvarchar](50) NULL,
        [date] [datetime] NULL
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
END";

$tables['WebsiteDB']['Downloads'] = "
IF NOT EXISTS ( SELECT * FROM [WebsiteDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'Downloads' )
BEGIN
    CREATE TABLE [WebsiteDB].[dbo].[Downloads](
        [heading] [nvarchar](50) NOT NULL,
        [message] [nvarchar](200) NOT NULL,
        [link] [nvarchar](max) NOT NULL,
        [linkText] [nvarchar](50) NOT NULL,
        [footer] [nvarchar](50) NULL,
        [date] [datetime] NULL,
        [id] [int] IDENTITY(1,1) NOT NULL
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
END";
?>