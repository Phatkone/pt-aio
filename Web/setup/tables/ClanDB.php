<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$tables['ClanDB']['CL'] = "
IF NOT EXISTS ( SELECT * FROM [ClanDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'CL' )
BEGIN
CREATE TABLE [ClanDB].[dbo].[CL](
	[IDX] [int] NOT NULL,
	[ClanName] [nvarchar](50) NOT NULL,
	[Note] [text] NOT NULL,
	[NoteCnt] [int] NOT NULL,
	[UserID] [nvarchar](50) NULL,
	[ClanZang] [nvarchar](50) NOT NULL,
	[Flag] [int] NOT NULL,
	[MemCnt] [int] NOT NULL,
	[MIconCnt] [int] NOT NULL,
	[RegiDate] [datetime] NOT NULL,
	[LimitDate] [datetime] NOT NULL,
	[DelActive] [char](1) NOT NULL,
	[PFlag] [int] NOT NULL,
	[KFlag] [int] NOT NULL,
	[Cpoint] [int] NULL,
	[CWin] [int] NULL,
	[CFail] [int] NULL,
	[ClanMoney] [bigint] NULL,
	[CNFlag] [int] NULL,
	[SiegeMoney] [bigint] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
END";

$tables['ClanDB']['CT'] = "
IF NOT EXISTS ( SELECT * FROM [ClanDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'CT' )
BEGIN
CREATE TABLE [ClanDB].[dbo].[CT](
	[SNo] [int] NULL,
	[ServerName] [varchar](50) NULL,
	[MIDX] [int] NULL,
	[ClanName] [varchar](50) NULL,
	[ClanJang] [int] NULL,
	[ClanImage] [varchar](50) NULL,
	[UserID] [varchar](50) NULL,
	[ChName] [varchar](50) NULL,
	[GPCode] [varchar](20) NULL,
	[LogonTime] [datetime] NULL,
	[IP] [varchar](20) NULL,
	[RNo] [int] NULL,
	[Flag] [int] NULL
) ON [PRIMARY];
END";

$tables['ClanDB']['LI'] = "
IF NOT EXISTS ( SELECT * FROM [ClanDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'LI' )
BEGIN
CREATE TABLE [ClanDB].[dbo].[LI](
	[IMG] [int] NOT NULL,
	[ID] [int] NOT NULL
) ON [PRIMARY];
END";

$tables['ClanDB']['SiegeMoneyTax'] = "
IF NOT EXISTS ( SELECT * FROM [ClanDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'SiegeMoneyTax' )
BEGIN
CREATE TABLE [ClanDB].[dbo].[SiegeMoneyTax](
	[idx] [bigint] NOT NULL,
	[mixing] [bigint] NULL,
	[aging] [bigint] NULL,
	[shop] [bigint] NULL,
	[poison1] [bigint] NULL,
	[poison2] [bigint] NULL,
	[poison3] [bigint] NULL,
	[force] [bigint] NULL,
	[warpgate] [bigint] NULL,
	[skill] [bigint] NULL,
	[total] [bigint] NULL,
	[tax] [bigint] NULL,
	[servername] [nvarchar](50) NULL
) ON [PRIMARY];
END";

$tables['ClanDB']['UL'] = "
IF NOT EXISTS ( SELECT * FROM [ClanDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'UL' )
BEGIN
CREATE TABLE [ClanDB].[dbo].[UL](
	[IDX] [int] NOT NULL,
	[MIDX] [int] NOT NULL,
	[userid] [nvarchar](50) NOT NULL,
	[ChName] [nvarchar](50) NOT NULL,
	[ChType] [int] NULL,
	[ChLv] [int] NULL,
	[ClanName] [nvarchar](50) NULL,
	[Permi] [char](2) NOT NULL,
	[JoinDate] [datetime] NOT NULL,
	[DelActive] [char](1) NOT NULL,
	[PFlag] [int] NOT NULL,
	[KFlag] [int] NOT NULL,
	[MIconCnt] [int] NOT NULL
) ON [PRIMARY];
END";