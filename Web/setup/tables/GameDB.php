<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$tables['GameDB']['Bless'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'Bless' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[Bless](
        [date] [datetime] NULL,
        [miconcnt] [int] NULL
    ) ON [PRIMARY];
END";

$tables['GameDB']['BossTime'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'BossTime' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[BossTime](
        [bosstime] [nvarchar](10) NOT NULL,
        [date] [datetime] NULL
    ) ON [PRIMARY];
END";

$tables['GameDB']['Classes'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'Classes' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[Classes](
        [intClass] [int] NULL,
        [namedClass] [nvarchar](20) NULL
    ) ON [PRIMARY];
END";

$tables['GameDB']['CharList'] = "
USE [GameDB];
IF OBJECT_ID('GameDB.dbo.CharList') IS NULL
BEGIN
EXEC('CREATE VIEW [CharList] AS (
	SELECT ROW_NUMBER() OVER (ORDER BY Account) AS RN, Account, CharName, CharClass, CharLevel, Field FROM GameDB.dbo.LevelList
)');
END";

$tables['GameDB']['LevelList'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'LevelList' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[LevelList](
        [IDX] [int] IDENTITY(1,1) NOT NULL,
        [Account] [nvarchar](50) NOT NULL,
        [CharName] [nvarchar](50) NOT NULL,
        [CharClass] [int] NOT NULL,
        [CharLevel] [int] NOT NULL,
        [Exp] [bigint] NOT NULL,
        [JobCode] [int] NULL,
        [Gold] [bigint] NULL,
        [Field_No] [int] NULL,
        [Field] [nvarchar](50) NULL,
        [Strength] [int] NULL,
        [Talent] [int] NULL,
        [Spirit] [int] NULL,
        [Agility] [int] NULL,
        [Health] [int] NULL,
        [t1_1] [int] NULL,
        [t1_2] [int] NULL,
        [t1_3] [int] NULL,
        [t1_4] [int] NULL,
        [t1_1t] [int] NULL,
        [t1_2t] [int] NULL,
        [t1_3t] [int] NULL,
        [t1_4t] [int] NULL,
        [t2_1] [int] NULL,
        [t2_2] [int] NULL,
        [t2_3] [int] NULL,
        [t2_4] [int] NULL,
        [t2_1t] [int] NULL,
        [t2_2t] [int] NULL,
        [t2_3t] [int] NULL,
        [t2_4t] [int] NULL,
        [t3_1] [int] NULL,
        [t3_2] [int] NULL,
        [t3_3] [int] NULL,
        [t3_4] [int] NULL,
        [t3_1t] [int] NULL,
        [t3_2t] [int] NULL,
        [t3_3t] [int] NULL,
        [t3_4t] [int] NULL,
        [t4_1] [int] NULL,
        [t4_2] [int] NULL,
        [t4_3] [int] NULL,
        [t4_4] [int] NULL,
        [t5_1] [int] NULL,
        [t5_2] [int] NULL,
        [t5_3] [int] NULL,
        [t5_4] [int] NULL,
     CONSTRAINT [pk_char_name] PRIMARY KEY CLUSTERED 
    (
        [CharName] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY];
END";

$tables['GameDB']['Fields'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'Fields' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[Fields](
        Field_No INT NOT NULL,
    	Field NVARCHAR(50) NOT NULL,
    	CONSTRAINT pk_field_no PRIMARY KEY CLUSTERED (Field_No)
        ) ON [PRIMARY];
END";

$tables['GameDB']['FieldDetails'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'FieldDetails' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[FieldDetails](
        [name] [nvarchar](50) NOT NULL,
        [file_name] [nvarchar](200) NULL,
        [max_monsters] [nvarchar](50) NULL,
        [spawn_interval] [nvarchar](50) NULL,
        [number_of_occurences] [nvarchar](50) NULL
    ) ON [PRIMARY];
END";

$tables['GameDB']['FieldMonsters'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'FieldMonsters' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[FieldMonsters](
        [Field] [nvarchar](50) NULL,
        [Monster] [nvarchar](50) NULL,
        [kMonster] [nvarchar](50) NULL,
        [SpawnRate] [nvarchar](50) NULL,
        [boss] [nvarchar](5) NULL,
        [minion] [nvarchar](100) NULL,
        [minion_count] [nvarchar](50) NULL,
        [boss_hours] [nvarchar](500) NULL
    ) ON [PRIMARY];
END";

$tables['GameDB']['ItemDetails'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'ItemDetails' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[ItemDetails](
        [code] [nvarchar](50) NOT NULL,
        [name] [nvarchar](50) NOT NULL,
        [integrity] [nvarchar](50) NULL,
        [weight] [nvarchar](50) NULL,
        [price] [nvarchar](50) NULL,
        [organic] [nvarchar](50) NULL,
        [fire] [nvarchar](50) NULL,
        [frost] [nvarchar](50) NULL,
        [lightning] [nvarchar](50) NULL,
        [undead] [nvarchar](50) NULL,
        [attack] [nvarchar](50) NULL,
        [attack_range] [nvarchar](50) NULL,
        [attack_rating] [nvarchar](50) NULL,
        [accuracy] [nvarchar](50) NULL,
        [critical] [nvarchar](50) NULL,
        [absorb] [nvarchar](50) NULL,
        [defence] [nvarchar](50) NULL,
        [block] [nvarchar](50) NULL,
        [speed] [nvarchar](50) NULL,
        [potion_count] [nvarchar](50) NULL,
        [hp_regen] [nvarchar](50) NULL,
        [stm_regen] [nvarchar](50) NULL,
        [mp_regen] [nvarchar](50) NULL,
        [hp_add] [nvarchar](50) NULL,
        [stm_add] [nvarchar](50) NULL,
        [mp_add] [nvarchar](50) NULL,
        [magic_apt] [nvarchar](50) NULL,
        [level] [nvarchar](50) NULL,
        [strength] [nvarchar](50) NULL,
        [spirit] [nvarchar](50) NULL,
        [talent] [nvarchar](50) NULL,
        [agility] [nvarchar](50) NULL,
        [health] [nvarchar](50) NULL,
        [spec_primary_class] [nvarchar](max) NULL,
        [spec_class] [nvarchar](max) NULL,
        [spec_attack] [nvarchar](50) NULL,
        [spec_attack_range] [nvarchar](50) NULL,
        [spec_attack_rating] [nvarchar](50) NULL,
        [spec_accuracy] [nvarchar](50) NULL,
        [spec_critical] [nvarchar](50) NULL,
        [spec_absorb] [nvarchar](50) NULL,
        [spec_defence] [nvarchar](50) NULL,
        [spec_block] [nvarchar](50) NULL,
        [spec_speed] [nvarchar](50) NULL,
        [spec_hp_regen] [nvarchar](50) NULL,
        [spec_stm_regen] [nvarchar](50) NULL,
        [spec_mp_regen] [nvarchar](50) NULL,
        [spec_hp_add] [nvarchar](50) NULL,
        [spec_stm_add] [nvarchar](50) NULL,
        [spec_mp_add] [nvarchar](50) NULL,
        [spec_magic_apt] [nvarchar](50) NULL,
        [special_hp_regen] [nvarchar](50) NULL,
        [special_stm_regen] [nvarchar](50) NULL,
        [special_mp_regen] [nvarchar](50) NULL
    ) ON [PRIMARY];
END";

$tables['GameDB']['MonsterDetails'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'MonsterDetails' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[MonsterDetails](
        [name] [nvarchar](50) NOT NULL,
        [true_name] [nvarchar](50) NULL,
        [file_name] [nvarchar](200) NOT NULL,
        [boss] [nvarchar](50) NULL,
        [model_size] [nvarchar](50) NULL,
        [level] [nvarchar](50) NULL,
        [health] [nvarchar](50) NULL,
        [attack] [nvarchar](50) NULL,
        [absorb] [nvarchar](50) NULL,
        [block] [nvarchar](50) NULL,
        [defence] [nvarchar](50) NULL,
        [attack_speed] [nvarchar](50) NULL,
        [attack_rating] [nvarchar](50) NULL,
        [special_attack_rate] [nvarchar](50) NULL,
        [attack_range] [nvarchar](50) NULL,
        [organic_resistance] [nvarchar](50) NULL,
        [lightning_resistance] [nvarchar](50) NULL,
        [ice_resistance] [nvarchar](50) NULL,
        [fire_resistance] [nvarchar](50) NULL,
        [mutant_resistance] [nvarchar](50) NULL,
        [magic_resistance] [nvarchar](50) NULL,
        [movement_speed] [nvarchar](50) NULL,
        [potion_count] [nvarchar](50) NULL,
        [quest_drop] [nvarchar](50) NULL,
        [experience] [nvarchar](50) NULL,
        [drop_count] [nvarchar](50) NULL,
        [drop_gold] [nvarchar](50) NULL,
        [drop_gold_rate] [nvarchar](50) NULL,
        [drop1] [nvarchar](max) NULL,
        [drop1_rate] [nvarchar](50) NULL,
        [drop2] [nvarchar](max) NULL,
        [drop2_rate] [nvarchar](50) NULL,
        [drop3] [nvarchar](max) NULL,
        [drop3_rate] [nvarchar](50) NULL,
        [drop4] [nvarchar](max) NULL,
        [drop4_rate] [nvarchar](50) NULL,
        [drop5] [nvarchar](max) NULL,
        [drop5_rate] [nvarchar](50) NULL,
        [drop6] [nvarchar](max) NULL,
        [drop6_rate] [nvarchar](50) NULL,
        [name_file] [nvarchar](50) NULL
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
END";

$tables['GameDB']['Monster'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'Monster' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[Monster](
        [ename] [nvarchar](50) NOT NULL,
        [kname] [nvarchar](50) NOT NULL,
        [boss] [nvarchar](50) NULL,
        [modelsize] [nvarchar](50) NULL,
        [attribute] [nvarchar](50) NULL,
        [shapefile] [nvarchar](100) NULL,
        [screenadjust] [nvarchar](50) NULL,
        [level] [int] NULL,
        [timelimit] [nvarchar](50) NULL,
        [grouping] [nvarchar](50) NULL,
        [intellect] [nvarchar](50) NULL,
        [nature] [nvarchar](50) NULL,
        [vision] [int] NULL,
        [hp] [int] NULL,
        [attack] [nvarchar](50) NULL,
        [absorb] [nvarchar](50) NULL,
        [block] [nvarchar](50) NULL,
        [defence] [int] NULL,
        [attackspeed] [int] NULL,
        [accuracy] [int] NULL,
        [specattackrate] [nvarchar](50) NULL,
        [size] [nvarchar](50) NULL,
        [attackrange] [nvarchar](50) NULL,
        [organic] [nvarchar](50) NULL,
        [lightning] [nvarchar](50) NULL,
        [ice] [nvarchar](50) NULL,
        [fire] [nvarchar](50) NULL,
        [poison] [nvarchar](50) NULL,
        [magic] [nvarchar](50) NULL,
        [race] [nvarchar](50) NULL,
        [movespeed] [nvarchar](50) NULL,
        [movetype] [nvarchar](50) NULL,
        [soundeffect] [nvarchar](50) NULL,
        [exp] [bigint] NULL,
        [eventitem] [nvarchar](50) NULL,
        [itemcount] [int] NULL,
        [namefile] [nvarchar](50) NULL
    ) ON [PRIMARY];
END";

$tables['GameDB']['MonsterDrops'] = "
IF NOT EXISTS ( SELECT * FROM [GameDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'MonsterDrops' )
BEGIN
    CREATE TABLE [GameDB].[dbo].[MonsterDrops](
        [monster] [nvarchar](50) NOT NULL,
        [droprate] [int] NOT NULL,
        [drops] [nvarchar](max) NULL
    ) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY];
END";

$tables['GameDB']['Field_Trigger'] = "
CREATE TRIGGER field_trigger ON [GameDB].[dbo].[LevelList]
AFTER UPDATE, INSERT
AS
BEGIN
	SET NOCOUNT ON;
	UPDATE LevelList
	SET Field = (SELECT f.Field FROM Fields f WHERE field_no = i.field_no)
	FROM LevelList l
	JOIN inserted i on l.field_no = i.field_no
	WHERE l.CharName = i.CharName;
END;";
?>