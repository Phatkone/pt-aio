<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$tables['LogDB']['AccountEmail'] = "
IF NOT EXISTS ( SELECT * FROM [LogDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'AccountEmail' )
BEGIN
    CREATE TABLE [LogDB].[dbo].[AccountEmail](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [account] [nvarchar](50) NOT NULL,
        [email] [nvarchar](50) NOT NULL,
        [ipAddress] [nvarchar](15) NOT NULL,
        [date] [datetime] NOT NULL
    ) ON [PRIMARY];
END";

$tables['LogDB']['NewAccount'] = "
IF NOT EXISTS ( SELECT * FROM [LogDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'NewAccount' )
BEGIN
    CREATE TABLE [LogDB].[dbo].[NewAccount](
        [account] [nvarchar](50) NOT NULL,
        [password] [nvarchar](50) NOT NULL,
        [ipAddress] [nvarchar](15) NOT NULL,
        [email] [nvarchar](100) NOT NULL,
        [date] [datetime] NOT NULL,
        [token] [nvarchar](50) NOT NULL
    ) ON [PRIMARY];
END";

$tables['LogDB']['Register'] = "
IF NOT EXISTS ( SELECT * FROM [LogDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'Register' )
BEGIN
    CREATE TABLE [LogDB].[dbo].[Register](
        [AccountName] [nvarchar](50) NOT NULL,
        [Date] [datetime] NULL,
        [Email] [nvarchar](50) NOT NULL,
        [ID] [int] IDENTITY(1,1) NOT NULL
    ) ON [PRIMARY];
END";

$tables['LogDB']['PassReset'] = "
IF NOT EXISTS ( SELECT * FROM [LogDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'PassReset' )
BEGIN
    CREATE TABLE [LogDB].[dbo].[PassReset](
        [id] [int] IDENTITY(1,1) NOT NULL,
        [ip] [nvarchar](16) NULL,
        [user] [nvarchar](50) NULL,
        [token] [nvarchar](60) NULL,
        [date] [datetime] NULL,
        [reset] [nvarchar](10) NULL
    ) ON [PRIMARY];
END";

$tables['LogDB']['ServerLogs'] = "
IF NOT EXISTS ( SELECT * FROM [LogDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'ServerLogs' )
BEGIN
    CREATE TABLE [LogDB].[dbo].[ServerLogs](
        [ID] [int] IDENTITY(1,1) NOT NULL,
        [Checksum] [nvarchar](100) NOT NULL,
        [Type] [nvarchar](50) NOT NULL,
        [Timestamp] [datetime] NULL,
        [Msg] [nvarchar](500) NULL,
     CONSTRAINT [pk_log_checksum] PRIMARY KEY CLUSTERED 
    (
        [Checksum] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY];
END";
?>