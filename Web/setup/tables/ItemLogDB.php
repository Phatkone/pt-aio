<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$tables['ItemLogDB']['il'] = "
IF NOT EXISTS ( SELECT * FROM [ItemLogDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'il' )
BEGIN
    CREATE TABLE [ItemLogDB].[dbo].[il](
        [userid] [nvarchar](50) NOT NULL,
        [charname] [nvarchar](50) NOT NULL,
        [ip] [nvarchar](15) NOT NULL,
        [flag] [int] NULL,
        [tuserid] [nvarchar](50) NULL,
        [tcharname] [nvarchar](50) NULL,
        [tmoney] [int] NULL,
        [tip] [nvarchar](15) NULL,
        [itemcount] [int] NULL,
        [itemcode] [bigint] NULL,
        [itemino] [int] NULL,
        [itemino_1] [int] NULL,
        [registday] [datetime] NULL
    ) ON [PRIMARY];
END";

$tables['ItemLogDB']['ItemLog'] = "
IF NOT EXISTS ( SELECT * FROM [ItemLogDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE 'ItemLog' )
BEGIN
    CREATE TABLE [ItemLogDB].[dbo].[ItemLog](
        [userid] [nvarchar](50) NOT NULL,
        [charname] [nvarchar](50) NOT NULL,
        [ip] [nvarchar](15) NOT NULL,
        [flag] [int] NULL,
        [tuserid] [nvarchar](50) NULL,
        [tcharname] [nvarchar](50) NULL,
        [tmoney] [int] NULL,
        [tip] [nvarchar](15) NULL,
        [itemcount] [int] NULL,
        [itemcode] [bigint] NULL,
        [itemino] [int] NULL,
        [itemino_1] [int] NULL,
        [registday] [datetime] NULL
    ) ON [PRIMARY];
END";
?>