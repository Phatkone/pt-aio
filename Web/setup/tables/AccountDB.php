<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));

foreach ($tables['AccountDB'] as $table)
{
    $pk = ($table == "AllGameUser") ? "AllUser_PK" : "{$table[0]}User_PK";
    $tables['AccountDB'][$table] =  "
    IF NOT EXISTS ( SELECT * FROM [AccountDB].[INFORMATION_SCHEMA].[TABLES] WHERE TABLE_NAME LIKE '{$table}' )
    BEGIN
        CREATE TABLE [AccountDB].[dbo].[{$table}](
            [userid] [varchar](32) NOT NULL,
            [Passwd] [varchar](32) NOT NULL,
            [GameCode] [varchar](10) NULL,
            [GPCode] [varchar](10) NULL,
            [RegistDay] [datetime] NULL,
            [DisuseDay] [datetime] NULL,
            [UsePeriod] [int] NULL,
            [inuse] [char](1) NOT NULL,
            [Grade] [char](1) NOT NULL,
            [EventChk] [char](1) NOT NULL,
            [SelectChk] [char](1) NOT NULL,
            [BlockChk] [char](1) NOT NULL,
            [SpecialChk] [char](1) NOT NULL,
            [ServerName] [varchar](50) NULL,
            [Credit] [char](1) NOT NULL,
            [ECoin] [money] NULL,
            [StartDay] [datetime] NULL,
            [LastDay] [datetime] NULL,
            [EditDay] [datetime] NULL,
            [RNo] [int] NULL,
            [DelChk] [char](1) NOT NULL,
            [SNo] [varchar](50) NULL,
            [Channel] [varchar](50) NULL,
            [BNum] [int] NULL,
            [MXServer] [varchar](50) NULL,
            [MXChar] [varchar](50) NULL,
            [MXType] [int] NULL,
            [MXLevel] [int] NULL,
            [MXExp] [bigint] NULL,
        CONSTRAINT [{$pk}] PRIMARY KEY CLUSTERED 
        (
            [userid] ASC
        ) 
        WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
        ) ON [PRIMARY];
    END";
}
?>