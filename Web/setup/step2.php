<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
require_once("setup/dbstructure.php");
$dbHost = (isset($_POST['dbHost'])) ? $_POST['dbHost'] : false;
$dbSA = (isset($_POST['dbSA'])) ? $_POST['dbSA'] : false;
$dbSAP = (isset($_POST['dbSAP'])) ? $_POST['dbSAP'] : false;
$dbUser = (isset($_POST['dbUser'])) ? $_POST['dbUser'] : false;
$dbPass = (isset($_POST['dbPass'])) ? $_POST['dbPass'] : false;
@session_start();
$_SESSION['dbHost'] = $dbHost;
$_SESSION['dbSA'] = $dbSA;
$_SESSION['dbSAP'] = $dbSAP;
$_SESSION['dbUser'] = $dbUser;
$_SESSION['dbPass'] = $dbPass;

if (file_exists('setup/temp.ini'))
{
    $stngs = parse_ini_file('setup/temp.ini');
    $dbHost = $stngs['dbHost'];
    $dbSA = $stngs['dbSA'];
    $dbSAP = $stngs['dbSAP'];
    $dbUser = $stngs['dbUser'];
    $dbPass = $stngs['dbPass'];
}

if ($dbHost && $dbSA && $dbSAP && $dbUser && $dbPass)
{
    file_put_contents('setup/temp.ini', "[SetupConfigs]
dbHost = {$dbHost}
dbSA = {$dbSA}
dbSAP = {$dbSAP}
dbUser = {$dbUser}
dbPass = {$dbPass}
    ");
}
elseif (!isset($sub[2]))
{
    @unlink('setup/temp.ini');
    header("Location: ?step/1");
}
//echo "Making the following Databases and Tables: <br /><br /><ul>";
/*foreach ($tables as $db => $tablelist)
{
    echo "<li class='parent'>{$db}<ul>";
    foreach ($tablelist as $table)
    {
        echo "<li>{$table}</li>";
    }
    echo "</ul></li><br />";
}
echo "</ul>";*/
if (isset($sub[2]) && $sub[2] == 'action')
{
    if ($dbHost && $dbSA && $dbSAP && $dbUser && $dbPass)
    {
        if ($sub[3] == "makeTable" || $sub[3] == "checkTable")
            $dbconn = new dbconn($dbHost, $dbSA, $dbSAP, $sub[4]);
        else
            $dbconn = new dbconn($dbHost, $dbSA, $dbSAP);
        $connect = $dbconn->connect();
        if ($connect === true)
        {
            switch ($sub[3])
            {
                case "checkUser":
                    $q = "SELECT count(loginname) as entries FROM master.sys.syslogins WHERE name = '{$dbUser}'";
                    $r = $dbconn->query($q);
                    if (is_array($r) && $r[0]['entries'] == 0)
                        print(json_encode(["Success" => "Not Exist"]));
                    elseif (is_array($r) && $r[0]['entries'] == 1)
                        print(json_encode(["Success" => "Exist"]));
                    else
                        error_log(print_r($r,true)."\r\n".$q);
                break;
                case "makeUser":
                    $q = "IF NOT EXISTS (SELECT loginname FROM master.sys.syslogins WHERE name = '{$dbUser}')
                        BEGIN 
                            CREATE LOGIN {$dbUser} WITH PASSWORD = '{$dbPass}', CHECK_POLICY = OFF;
                        END";
                    $r = $dbconn->nonQuery($q);
                    if ($r !== false)
                        print(json_encode(["Success" => "Created DB User: {$dbUser}"]));
                    else
                        print(json_encode(["Error" => "Error Creating DB User: {$dbUser}"]));
                break;
                case "checkDB":
                    $q = "SELECT COUNT(*) as entries FROM master.sys.databases WHERE NAME LIKE '{$sub[4]}'";
                    $r = $dbconn->query($q);
                    if (is_array($r) && $r[0]['entries'] == 0)
                        print(json_encode(["Success" => "Not Exist"]));
                    elseif (is_array($r) && $r[0]['entries'] == 1)
                        print(json_encode(["Success" => "Exist"]));
                    else
                        error_log(print_r($r,true)."\r\n".$q);
                break;
                case "makeDB":
                    $q = "CREATE DATABASE [{$sub[4]}]";
                    $r = $dbconn->nonQuery($q);
                    if ($r !== false)
                    {
                        $q = "USE [{$sub[4]}];
                                CREATE USER {$sub[5]} FROM LOGIN {$sub[5]};
                                GRANT SELECT, UPDATE, DELETE, INSERT, EXECUTE TO {$sub[5]};";
                        $dbconn->nonQuery($q);
                        print(json_encode(["Success" => "Created Database: {$sub[4]}"]));
                    }
                    else
                        print(json_encode(["Error" => "Error Creating Database: {$sub[4]}"]));
                break;
                case "checkTable":
                    $q = "SELECT COUNT(*) as entries FROM {$sub[4]}.INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '{$sub[5]}'";
                    $r = $dbconn->query($q);
                    if (is_array($r) && $r[0]['entries'] == 0)
                        print(json_encode(["Success" => "Not Exist"]));
                    elseif (is_array($r) && $r[0]['entries'] == 1)
                        print(json_encode(["Success" => "Exist"]));
                    else
                        error_log(print_r($r,true)."\r\n".$q);
                break;
                case "makeTable":
                    require_once("tables/{$sub[4]}.php");
                    $q = $tables[$sub[4]][$sub[5]];
                    $r = $dbconn->nonQuery($q);
                    //if ($db == 'AccountDB')
                    //    error_log(print_r($uri,true)."{$sub[5]} \r\n $db \r\n $table \r\n $q\r\n");
                    if ($r !== false)
                        print(json_encode(["Success" => "Created DB Table: {$sub[5]}"]));
                    else
                        print(json_encode(["Error" => "Error Creating DB Table: {$sub[5]}"])) . error_log($r);
                break;
                case "checkView":
                    $q = "SELECT COUNT(*) as entries FROM {$sub[4]}.INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME LIKE '{$sub[5]}'";
                    $r = $dbconn->query($q);
                    if (is_array($r) && $r[0]['entries'] == 0)
                        print(json_encode(["Success" => "Not Exist"]));
                    elseif (is_array($r) && $r[0]['entries'] == 1)
                        print(json_encode(["Success" => "Exist"]));
                    else
                        error_log(print_r($r,true)."\r\n".$q);
                break;
                case "checkProcedure":
                    $q = "SELECT COUNT(*) as entries FROM {$sub[4]}.sys.objects WHERE type = 'P' AND name LIKE '{$sub[5]}'";
                    $r = $dbconn->query($q);
                    if (is_array($r) && $r[0]['entries'] == 0)
                        print(json_encode(["Success" => "Not Exist"]));
                    elseif (is_array($r) && $r[0]['entries'] == 1)
                        print(json_encode(["Success" => "Exist"]));
                    else
                        error_log(print_r($r,true)."\r\n".$q);
                break;
                case "checkTrigger":
                    $q = "SELECT COUNT(*) as entries FROM {$sub[4]}.sys.triggers WHERE name LIKE '{$sub[5]}'";
                    $r = $dbconn->query($q);
                    if (is_array($r) && $r[0]['entries'] == 0)
                        print(json_encode(["Success" => "Not Exist"]));
                    elseif (is_array($r) && $r[0]['entries'] == 1)
                        print(json_encode(["Success" => "Exist"]));
                    else
                        error_log(print_r($r,true)."\r\n".$q);
                break;
                default: 
                    print("Error");
            }
        }
        else
        {
            print($connect['Error']);
        }
    }
    else
    {
        print("Missing Connection Info");
    }

}
elseif ($sub[0] == 'step')
{
    header("Content-Type: text/html; charset=utf-8\n\n");
    if ($dbHost && $dbSA && $dbSAP && $dbUser && $dbPass)
    {
        $dbconn = new dbconn($dbHost, $dbSA, $dbSAP);
        $connect = $dbconn->connect();
        if ($connect !== true)
        {
            echo "<h2>Step 2</h2>
                <h4>Error connecting to {$dbHost} with {$dbSA} <font color='#CC0000'>&#10008</font></h4><p>{$connect['Error']}</p>
                <input type='submit' style='margin:0 auto;' onClick='window.location.href=\"?setup::step/1\"' value='Return'/>";
            $dbconn = false;
        }
        else
        {
            echo "<h2>Step 2</h2> <b>Connected to {$dbHost} <font color='#00CC00'>&check;</font></b>";
            echo "<table class='setuptable'><tr><th>Task</th><th>Result</th></tr>";
            echo "<tr><td>Create DB User: {$dbUser}</td><td id='dbt1'>Pending</td></tr>";
            echo "<script type='text/javascript'>makeUser('{$dbUser}','{$dbPass}',1);</script>";
            $c = 2;
            foreach ($tables as $db => $tablelist)
            {
                echo "<tr><td>Create DB: {$db}</td><td id='dbt{$c}'>Pending</td></tr>";
                echo "<script type='text/javascript'>makeDB('{$db}','{$dbUser}',{$c});</script>";
                $c++;
                foreach ($tablelist as $table)
                {
                    echo "<tr><td>Create DB Table: [{$db}].[dbo].[{$table}]</td><td id='dbt{$c}'>Pending</td></tr>";
                    echo "<script type='text/javascript'>makeTable('{$db}','{$table}',{$c});</script>";
                    $c++;
                }
                if (in_array($db,array_keys($views)))
                {
                    foreach ($views[$db] as $view)
                    {
                        echo "<tr><td>Create DB View: [{$db}].[dbo].[{$view}]</td><td id='dbt{$c}'>Pending</td></tr>";
                        echo "<script type='text/javascript'>makeView('{$db}','{$view}',{$c});</script>";
                        $c++;
                    }
                }
                if (in_array($db,array_keys($procedures)))
                {
                    foreach ($procedures[$db] as $proc)
                    {
                        echo "<tr><td>Create DB Procedure: [{$db}].[dbo].[{$proc}]</td><td id='dbt{$c}'>Pending</td></tr>";
                        echo "<script type='text/javascript'>makeProcedure('{$db}','{$proc}',{$c});</script>";
                        $c++;
                    }
                }
                if (in_array($db,array_keys($triggers)))
                {
                    foreach ($triggers[$db] as $t)
                    {
                        echo "<tr><td>Create DB Trigger: [{$db}].[dbo].[{$t}]</td><td id='dbt{$c}'>Pending</td></tr>";
                        echo "<script type='text/javascript'>makeTrigger('{$db}','{$t}',{$c});</script>";
                        $c++;
                    }
                }
            }
            echo "<tr><td colspan=2><input type='Submit' value='Back' onClick='window.location.href = \"?setup::step/1\";' />&nbsp<input type='Submit' value='Continue' onClick='window.location.href = \"?setup::step/3\";' /></td></tr>";
            echo "</table>";
            /*foreach ($databases as $db)
            {
                require_once("setup/tables/{$db}.php");
            }*/
        }
        $dbconn->close();
    }
    else
    {
        header("Location: ?step/1");
    }
}
?>