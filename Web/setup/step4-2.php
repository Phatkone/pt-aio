<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
if ($sub[3] == "fields")
{
    require_once('setup/fields.php');
    $db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass'], "GameDB");
    if ($db->connect() !== true)
        print(json_encode(['Error' => "Unable to connect to Database"]));
    else
    {
        $q = "INSERT INTO [Fields] VALUES
        ";
        foreach ($fields as $i => $f)
        {
    		$f = str_replace("'","''",$f);
            $q .= "({$i},'{$f}'),\n";
        }
        $q = rtrim(rtrim($q,"\n"), ",");
        $r = $db->nonQuery($q);
        if ($r)
            print(json_encode(["Success" => "Populated Fields Table"]));
        else
            print(json_encode(["Error" => "Error Populating Fields Table, Check Error Log"]));
    }
}
elseif ($sub[3] == "items")
{
    require_once('setup/items.php');
}
elseif ($sub[3] == "monsters")
{
    require_once('setup/monsters.php');    
}
?>