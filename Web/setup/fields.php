<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$fields = array(
0x0 => 'Acasia Forest',
0x1 => 'Bamboo Forest',
0x2 => 'Garden Of Freedom',
0x3 => 'Ricarten',
0x4 => 'Refuge Of The Ancients',
0x5 => 'Castle Of The Lost',
0x6 => 'Ruinen',
0x7 => 'Cursed Land',
0x8 => 'Forgotten Land',
0x9 => 'Navisko',
0xa => 'Oasis',
0xb => 'Battlefield Of The Ancients',
0xc => 'Forbidden Land',
0xd => 'Dungeon 1',
0xe => 'Dungeon 2',
0xf => 'Dungeon 3',
0x10 => 'GM Room',
0x11 => 'Forest Of Spirits',
0x12 => 'Land Of Dusk',
0x13 => 'Valley Of Tranquility',
0x14 => 'Road To The Wind',
0x15 => 'Pillai',
0x16 => 'Cursed Temple 1',
0x17 => 'Cursed Temple 2',
0x18 => 'Cave Of Mushrooms',
0x19 => 'Cave Of Beehive',
0x1a => 'Sanctuary Of Darkness',
0x1b => 'Railway Of Chaos',
0x1c => 'Heart Of Perum',
0x1d => 'Eura',
0x1e => 'SOD Stage',
0x1f => 'Gallubia Valley',
0x20 => 'Fury Battle Stage',
0x21 => 'BlessCastle',
0x22 => 'Greedy Lake',
0x23 => 'Frozen Sanctuary',
0x24 => 'Kelvezu\' Lair',
0x25 => 'Land Of Chaos',
0x26 => 'Lost Temple',
0x27 => 'Ghost Castle Stage',
0x28 => 'Endless Tower 1',
0x29 => 'Endless Tower 2',
0x2a => 'Cursed Temple 3',
0x2b => 'Endless Tower 3',
0x2c => 'Ice Mine 1',
0x2d => 'SOD Stage 2',
0x2e => 'Secret Laboratory',
0x2f => 'Lizard Stage',
0x30 => 'Ancient Weapon',
0x31 => 'Sea Of Abyss',
0x32 => 'Battle Town',
0x33 => 'Mystery Desert 1',
0x34 => 'Mystery Desert 2',
0x35 => 'Mystery Desert 3',
0x36 => 'Mystery Forest 3',
0x37 => 'Mystery Forest 2',
0x38 => 'Mystery Forest 1',
0x39 => 'Atlantis Town',
);
?>