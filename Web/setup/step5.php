<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
if (file_exists('setup/temp.ini'))
    unlink('setup/temp.ini');

// Push charlist and log parser settings files as downloads <!-- HERE -->
?>
<h2>Setup Complete</h2>
<form action='?setup::step/6' method='POST'>
<table><tr><td>Administrator account name:</td><td><input type='text' name='admin' placeholder='Admin' required/></td></tr>
<tr><td>Administrator password:</td><td><input type='password' name='adminpass' required/></td></tr>
<tr><td colspan=2><input type='submit' value='Finish' /></td></tr></table>