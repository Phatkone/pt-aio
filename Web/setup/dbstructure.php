<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$databases = ['AccountDB','ClanDB','GameDB','ItemLogDB','LogDB','SoDDB','WebsiteDB'];
$tables = [
    'AccountDB' => 
        ['AllGameUser',
        'AGameUser',
        'BGameUser',
        'CGameUser',
        'DGameUser',
        'EGameUser',
        'FGameUser',
        'GGameUser',
        'HGameUser',
        'IGameUser',
        'JGameUser',
        'KGameUser',
        'LGameUser',
        'MGameUser',
        'NGameUser',
        'OGameUser',
        'PGameUser',
        'QGameUser',
        'RGameUser',
        'SGameUser',
        'TGameUser',
        'UGameUser',
        'VGameUser',
        'WGameUser',
        'XGameUser',
        'YGameUser',
        'ZGameUser'
    ],
    'ClanDB' => [
        'CL',
        'CT',
        'LI',
        'SiegeMoneyTax',
        'UL'
    ],
    'GameDB' => [
        'Bless',
        'BossTime',
        'Classes',
        'LevelList',
        'Fields',
        'FieldDetails',
        'FieldMonsters',
        'ItemDetails',
        'MonsterDetails',
        'Monster',
        'MonsterDrops'
    ],
    'ItemLogDB' => [
        'il'
        //'ItemLog'
    ],
    'LogDB' => [
        'AccountEmail',
        'NewAccount',
        'Register',
        'PassReset',
        'ServerLogs'
    ],
    'SoDDB' => [
        'SoD2BBSSub',
        'SoD2Config',
        'SoD2ConfigLog',
        'SoD2Notice',
        'SoD2RecBySandurr',
        'SoD2TaxLog',
        'SoDAllTime',
        'SoDBBSMain',
        'Temp',
        'UserBlackList'
    ],
    'WebsiteDB' => [
        'AdminNotices',
        'Downloads'
        ]
];

$views = ['GameDB' => ['CharList']];
$procedures = ['SoDDB' => ['makeSODMonthlyTables']];
$triggers = ['GameDB' => ['Field_Trigger']];
?>