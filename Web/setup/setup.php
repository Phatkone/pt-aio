<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
if (file_exists('common/config.lock'))
    header('Location: ?web');

if (isset($sub[2]) && $sub[2] == 'action')
{
    $step = isset($sub[1]) ? $sub[1] : 1;
    if (file_exists("setup/step{$step}.php"))
        require_once("setup/step{$step}.php");
}
else
{
foreach ($headers as $k => $v)
{
    if ($k !== "LOCATION")
    {
        header_remove(ucwords(strtolower($k)));
        header($v);
    }
}
?>
<html lang='en'>
<head>
<title>
    Pristontale Website and Database Setup | By Phatkone
</title>
<link rel='stylesheet' href='/web/css/jquery-ui.min.css' />
<link rel='stylesheet' href='/web/css/setup.css' />
<script type='text/javascript' src='/web/js/jquery-2-1-1.min.js'></script>
<script type='text/javascript' src='/web/js/jquery-ui.min.js'></script>
<script type='text/javascript' src='/web/js/setup.js'></script>
<link rel='icon' type='image/x-icon' href='/web/images/favicon.ico' />
<link rel='shortcut icon' type='image/x-icon' href='/web/images/favicon.ico' />
</head>
<body class='<?=$theme?>'>
<h2 class='heading'> Pristontale Website and Database Setup </h2>
<div class='body'>
<?php
    $step = isset($sub[1]) ? $sub[1] : 1;
    if (file_exists("setup/step{$step}.php"))
        require_once("setup/step{$step}.php");
?>
</div>
<footer class='footer'>All rights reserved &copy Kilroy Inc</footer>
<?php } ?>