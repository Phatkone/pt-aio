<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$invalid = (isset($_GET['invalid'])) ? "<h4 style='color:red'>Please populate all fields'</h2>" : "";
@session_start();
$dbHost = (isset($_SESSION['dbHost'])) ? $_SESSION['dbHost'] : "";
$dbSA = (isset($_SESSION['dbSA'])) ? $_SESSION['dbSA'] : "";
$dbSAP = (isset($_SESSION['dbSAP'])) ? $_SESSION['dbSAP'] : "";
$dbUser = (isset($_SESSION['dbUser'])) ? $_SESSION['dbUser'] : "";
$dbPass = (isset($_SESSION['dbPass'])) ? $_SESSION['dbPass'] : "";
$timezone = (isset($_SESSION['timezone'])) ? $_SESSION['timezone'] : "";
if ($webhost == "IIS") 
{
    if (isset($_SERVER["IIS_UrlRewriteModule"]))
        $rewrite = "<font color='00CC00'>IIS Mod Rewrite V:{$_SERVER["IIS_UrlRewriteModule"]} Is Enabled</font>";
    else
        $rewrite = "<font color='CC0000'>IIS Mod Rewrite Is Not Enabled, Please install and start again.</font>";
}
else
{
    ob_start();
    php_info();
    $inf = ob_get_contents();
    ob_get_clean();
    if (stripos($inf, "mod_rewrite"))
        $rewrite = "<font color='00CC00'>Apache Mod Rewrite Is Enabled</font>";
    else
        $rewrite = "<font color='CC0000'>Apache Mod Rewrite Is Not Enabled, Please install and start again.</font>";
}
?>
<h3>Step 1</h3>
<?=$invalid?>
<p><form action='?setup::step/2' method='POST'>
<table>
<tr><td>Database:</td><td><input type='text' name='dbHost' placeholder='PT-SERVER\\SQLEXPRESS' value='<?=$dbHost?>' /></td></tr>
<tr><td>DB Administrator User:</td><td><input type='text' name='dbSA' placeholder='SA' value='<?=$dbSA?>' /></td></tr>
<tr><td>DB Admin Password:</td><td><input type='password' name='dbSAP' placeholder='****' value='<?=$dbSAP?>' /></td></tr>
<tr><td>DB Service Account:</td><td><input type='text' name='dbUser' placeholder='svcsql' value='<?=$dbUser?>' /></td></tr>
<tr><td>DB Service Account Password:</td><td><input type='password' name='dbPass' placeholder='****' value='<?=$dbPass?>' /></td></tr>
<tr><td colspan=2><input type='Submit' value='Next' /></td></tr>
</form>