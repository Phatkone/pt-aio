<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$admin = (isset($_POST['admin'])) ? $_POST['admin'] : null;
$adminpass = (isset($_POST['adminpass'])) ? $_POST['adminpass'] : null;
if ($admin !== null)
{
    $ladmin = strtolower($admin);
    file_put_contents("common/admins.php", 
"<?php
if (!defined('PT'))
    die(header(\"HTTP/1.0 404 Not Found\"));
\$admins[] = '{$ladmin}';
?>");
    $db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass'], 'AccountDB');
    if ($db->connect() === true)
    {
        $db->nonQuery("INSERT INTO accountdb.dbo.AllGameUser 
            ([userid],[Passwd],[RegistDay],[DisuseDay],[inuse],[Grade],[EventChk],[SelectChk],[BlockChk],[SpecialChk],[Credit],[DelChk],[Channel]) 
                VALUES 
            ('{$admin}','{$adminpass}',GETDATE(),'12-12-2035','0','U','0','0','0','0','0','0','127.0.0.1')"
            );
        $db->nonQuery("INSERT INTO accountdb.dbo.{$admin[0]}GameUser 
            ([userid],[Passwd],[RegistDay],[DisuseDay],[inuse],[Grade],[EventChk],[SelectChk],[BlockChk],[SpecialChk],[Credit],[DelChk],[Channel]) 
                VALUES 
            ('{$admin}','{$adminpass}',GETDATE(),'12-12-2035','0','U','0','0','0','0','0','0','127.0.0.1')"
            );
        file_put_contents('common/config.lock',"");
        header("Location: ?web");
    }
    else
    {
        ?>
            <h2>Step 6</h2>
            <p> An error occurred creating the admin account. Please check the error log and try again </p>
        <?php
    }
}
else
{
    header('Location: ?setup::step/5');
}
?>