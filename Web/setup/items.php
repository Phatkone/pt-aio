<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
ini_set("max_execution_time", 0);
require_once("common\\item.class.php");
$dir = $configs['serverDirectory'].'\\gameserver\\openitem\\';
$items = scandir($dir);
$conn = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass'], 'GameDB');
$errors = false;
if ($conn->connect() !== true)
	die(json_encode(["Error" => "Error connecting to database"]));
if (is_dir($dir) && $files = scandir($dir))
{
    foreach ($items as $file)
    {
        if (is_file($dir.$file))
        {
            $item = new item($dir.$file);
            if ($item->name !== "" && $item->name !== null)
            {
                $item->name = str_replace("'","''",$item->name);
                $keys = [];
                $vals = [];
                foreach ($item as $k => $v)
                {
                    $keys[] = $k;
                    $vals[] = $v;
                }
                $str = "INSERT INTO GameDB.dbo.ItemDetails (";
                foreach ($keys as $k)
                {
                    $str .= "[$k],";
                }
                $str = substr($str, 0, strlen($str)-1);
                $str .= ") VALUES (";
                foreach ($vals as $v)
                {
                    $str .= "'$v',";
                }
                $str = substr($str, 0, strlen($str)-1);
                $str .= ");";
                if ($conn->nonQuery($str) === false)
                {
                    $errors = true;
                    error_log("Error inserting {$str}");
                }
            }
        }
    }
    $conn->close();
    if ($errors)
        die(json_encode(["Success" => "Item detail population succeeded with errors"]));
    else
        die(json_encode(["Success" => "Successfully populated item details"]));
}
else
{
    die(json_encode(["Error" => "Unable to open directory {$dir}"]));
}
?>