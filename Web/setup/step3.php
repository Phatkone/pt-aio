<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
@session_start();
?>
<h2>General Settings</h2>
<p><form action='?setup::step/4' method='POST'><table id='settingsTable'>
<tr><td colspan=2><b>Webpage Configuration</b></td></tr>
<tr><td>Theme</td><td><select name='theme'>
    <option value='Light'>Light</option>
    <option value='dark'>Dark</option>
    <option value='contrast'>High Contrast</option>
</td></tr>
<tr><td>Timezone:</td><td><select name='timezone' value='{$timezone}'>";
<?php
$zones = timezone_identifiers_list();
foreach ($zones as $tz)
{
    echo "<option value='{$tz}'>{$tz}</option>";
}
?>
</select></td></tr>
<tr><td>PT Server Directory</td><td><input type='text' name='serverDir' placeholder='D:\PT-Server' /></td></tr>
<tr><td>PT Server Address</td><td><input type='text' name='serverAddress' placeholder='127.0.0.1' /></td></tr>
<tr><td>PT Server Port</td><td><input type='text' name='serverPort' placeholder='10007' /></td></tr>
<tr><td colspan=2><b>Email Configuration</b></td></tr>
<tr><td>Use Email Verification</td><td><input type='checkbox' name='verification' id='emailChecked' /></td></tr>
<tr class='email'><td>Email (SMTP) Server</td><td><input type='text' name='smtpServer' placeholder='ssl://smtp.gmail.com'/></td></tr>
<tr class='email'><td>Email (SMTP) Server Port</td><td><input type='text' name='smtpPort' placeholder='443'/></td></tr>
<tr class='email'><td>Email Account</td><td><input type='text' name='email' placeholder='rzaptale@gmail.com'/></td></tr>
<tr class='email'><td>Email Password</td><td><input type='password' name='smtpPass' /></td></tr>
<tr class='email'><td>Sender Address</td><td><input type='text' name='sender' placeholder='RZA-PT <rzaptale@gmail.com>'/></td></tr>
<tr class='email'><td>Site URL</td><td><input type='text' name='url' placeholder='http://ptserver.com'/></td></tr>
<tr><td colspan=2><input type='Submit' value='Back' class='stop' onClick=' window.location.href = "?setup::step/2";'>&nbsp<input type='Submit' value='Continue' /></td></tr>
</table>
</form>