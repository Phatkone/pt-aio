<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
require_once('admins.php');
$loginMsg = "";
if (isset($_POST['uname'])) 
{ 
    $uname = $_POST['uname'];
    $_SESSION['uname'] = $uname;
}
elseif (isset($_SESSION['uname']))
{
    $uname = $_SESSION['uname'];
}
else
{
    $uname = "";
}
if (isset($_POST['upwd'])) 
{ 
    $upwd = $_POST['upwd'];
    $_SESSION['upwd'] = $upwd;
}
elseif (isset($_SESSION['upwd']))
{
    $upwd = $_SESSION['upwd'];
}
else
{
    $upwd = "";
}
$verified = (isset($_SESSION['logged_in'])) ? true : false ;
if (in_array(strtolower($uname),$admins))
    $admin = true;
if ($uname !== "" && $upwd !== "")
{
    $db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass'], "AccountDB");
    if ($db->Connect() === true)
    {
        $query = "SELECT COUNT(*) AS entries FROM [AccountDB].[dbo].[AllGameUser] WHERE UserID LIKE '{$uname}' AND Passwd='{$upwd}'";
        $r = $db->query($query);
        if ($r[0]['entries'] == 1)
        {
            $verified = true;
            $_SESSION['logged_in'] = true;
        }
        else
        {
            unset($_SESSION['uname']);
            unset($_SESSION['upwd']);
            unset($_SESSION['logged_in']);
            $uname = "";
            $upwd = "";
            $verified = false;
            $loginMsg = popup('Incorrect Credentials', 'Please check username and password and try again.');
        }
    }
    else
    {
        $loginMsg = popup('Uh-Oh!', "Error connecting to database.");
    }
}

if (isset($sub[0]) && $sub[0] == "logout")
{
    $_SESSION[] = "";
	unset($_SESSION['uname']);
	unset($_SESSION['upwd']);
	unset($_SESSION['logged_in']);
    $uname = "";
    $upwd = "";
	$verified = false;
	session_unset();
	session_destroy();
	if (ini_get("session.use_cookies")) {
		$params = session_get_cookie_params();
		setcookie(session_name(), '', time() - 42000,
			$params["path"], $params["domain"],
			$params["secure"], $params["httponly"]
		);
	}
	echo '<meta http-equiv="Refresh" content="0;url=/?">';
	exit;
}
?>