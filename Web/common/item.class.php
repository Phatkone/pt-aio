<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
class item {
	public $code;
	public $name;
	public $integrity;
	public $weight;
	public $price;
	public $organic;
	public $fire;
	public $frost;
	public $lightning;
	public $undead;
	public $attack;
	public $attack_range;
	public $attack_rating;
	public $accuracy;
	public $critical;
	public $absorb;
	public $defence;
	public $block;
	public $speed;
	public $potion_count;
	public $hp_regen;
	public $stm_regen;
	public $mp_regen;
	public $hp_add;
	public $stm_add;
	public $mp_add;
	public $magic_apt;
	public $level;
	public $strength;
	public $spirit;
	public $talent;
	public $agility;
	public $health;
	public $spec_primary_class;
	public $spec_class;
	public $spec_attack;
	public $spec_attack_range;
	public $spec_attack_rating;
	public $spec_accuracy;
	public $spec_critical;
	public $spec_absorb;
	public $spec_defence;
	public $spec_block;
	public $spec_speed;
	public $spec_hp_regen;
	public $spec_stm_regen;
	public $spec_mp_regen;
	public $spec_hp_add;
	public $spec_stm_add;
	public $spec_mp_add;
	public $spec_magic_apt;
	public function __construct($file)
	{
		$array = array(
			"*NAME" => "name",
			"*Name" => "name",
			"*코드" => "code",
			"*내구력" => "integrity",
			"*무게" => "weight",
			"*가격" => "price",
			"*생체" => "organic",
			"*불" => "fire",
			"*냉기" => "frost",
			"*번개" => "lightning",
			"*독" => "undead",
			"*공격력" => "attack",
			"*사정거리" => "attack_range",
			"*공격속도" => "attack_rating",
			"*명중력" => "accuracy",
			"*크리티컬" => "critical",
			"*흡수력" => "absorb",
			"*방어력" => "defence",
			"*블럭율" => "block",
			"*이동속도" => "speed",
			"*보유공간" => "potion_count",
			"*생명력재생" => "hp_regen",
			"*기력재생" => "stm_regen",
			"*근력재생" => "mp_regen",
			"*생명력추가" => "hp_add",
			"*기력추가" => "stm_add",
			"*근력추가" => "mp_add",
			"*마법기술숙련도" => "magic_apt",
			"*레벨" => "level",
			"*힘" => "strength",
			"*정신력" => "spirit",
			"*재능" => "talent",
			"*민첩성" => "agility",
			"*건강" => "health",
			"**특화랜덤" => "spec_class",
			"**특화" => "spec_primary_class",
			"*생명력상승" => "special_hp_regen",
			"*기력상승" => "special_stm_regen",
			"*근력상승" => "special_mp_regen",
			"**생체" => "spec_organic",
			"**불" => "spec_fire",
			"**냉기" => "spec_frost",
			"**번개" => "spec_lightning",
			"**독" => "spec_undead",
			"**공격력" => "spec_attack",
			"**사정거리" => "spec_attack_range",
			"**공격속도" => "spec_attack_rating",
			"**명중력" => "spec_accuracy",
			"**크리티컬" => "spec_critical",
			"**흡수력" => "spec_absorb",
			"**방어력" => "spec_defence",
			"**블럭율" => "spec_block",
			"**이동속도" => "spec_speed",
			"**보유공간" => "spec_potion_count",
			"**생명력추가" => "spec_hp_add",
			"**기력추가" => "spec_stm_add",
			"**근력추가" => "spec_mp_add",
			"**생명력재생" => "spec_hp_regen",
			"**기력재생" => "spec_stm_regen",
			"**근력재생" => "spec_mp_regen",
		);
		if (!is_file($file))
			return("Not a file...");
		$lines = file($file);
		foreach ($lines as $line)
		{
			$match = false;
			// echo "$line <br />";
			$line = mb_convert_encoding($line, "UTF-8", "EUC-KR");
			// echo "$line <br />";
			foreach ($array as $k => $v)
			{
				// echo strlen($k)." - $k => $v <br />";
				if ($match) 
					continue;
				// echo substr($line,0,strlen($k))."<br />";
				if (substr($line, 0, strlen($k)) == $k)
				{
					// $line = str_replace("	"," ",$line);
					$line = trim(str_replace($k,"",$line));
					$line = preg_replace("/\s+/"," ",$line);
					/*if ($v == "spec_class" || $v == "primary_spec_class")
						$line = str_replace(" ",", ",$line);
					elseif ($v !== "name")
						$line = str_replace(" "," - ",$line); */
					$this->$v = $line;
					$match = true;
					continue;
				}
			}
		}
		if (!empty($this->code)) $this->code = strtolower(str_replace('"',"",$this->code));
		if (!empty($this->name)) $this->name = ucwords(str_replace("'","''",str_replace('"',"",$this->name)));
		if (!empty($this->integrity)) $this->integrity = str_replace(' '," - ",$this->integrity);
		if (!empty($this->organic)) $this->organic = str_replace(' '," - ",$this->organic);
		if (!empty($this->fire)) $this->fire = str_replace(' '," - ",$this->fire);
		if (!empty($this->frost)) $this->frost = str_replace(' '," - ",$this->frost);
		if (!empty($this->lightning)) $this->lightning = str_replace(' '," - ",$this->lightning);
		if (!empty($this->undead)) $this->undead = str_replace(' '," - ",$this->undead);
		if (!empty($this->absorb)) $this->absorb = str_replace(' '," - ",$this->absorb);
		if (!empty($this->spec_absorb)) $this->spec_absorb = str_replace(' '," - ",$this->spec_absorb);
		if (!empty($this->defence)) $this->defence = str_replace(' '," - ",$this->defence);
		if (!empty($this->spec_defence)) $this->spec_defence = str_replace(' '," - ",$this->spec_defence);
		if (!empty($this->spec_class)) $this->spec_class = str_replace(' ',", ",$this->spec_class);
		if (!empty($this->block)) $this->block = str_replace(' '," - ",$this->block);
		if (!empty($this->accuracy)) $this->accuracy = str_replace(' '," - ",$this->accuracy);
		if (!empty($this->attack)) $this->attack = str_replace(' '," - ",$this->attack);
		$count = substr_count($this->attack,"-");
		if ($count == 3)
		{
			$explode = explode("-",$this->attack);
			$this->attack = $explode[0]." - ".$explode[1]." / ".$explode[2]." - ".$explode[3];
		}
	}
}
?>
