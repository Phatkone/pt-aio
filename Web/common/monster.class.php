<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
class monster {
	public $monfile;
	public $ename;
	public $kname;
	public $boss;
	public $modelsize;
	public $attribute;
	public $shapefile;
	public $screenadjust;
	public $level = 0;
	public $timelimit;
	public $grouping;
	public $intellect;
	public $nature;
	public $vision = 0;
	public $hp = 0;
	public $attack;
	public $absorb = 0;
	public $block = 0;
	public $defence = 0;
	public $attackspeed = 0;
	public $accuracy = 0;
	public $specattackrate;
	public $size;
	public $attackrange;
	public $organic;
	public $lightning;
	public $ice;
	public $fire;
	public $poison;
	public $magic;
	public $race;
	public $movespeed;
	public $movetype;
	public $soundeffect;
	public $exp = 0;
	public $eventitem;
	public $itemcount = 0;
	public $nodrop;
	public $moneydrop;
	public $drops;
	public $namefile;
	
	public function __construct($file = "")
	{
		$array = [
			"*이름" => "kname",
			"*Name" => "ename",
			"*레벨" => "level",
			"*두목" => "boss",
			"*모델크기" => "modelsize",
			"*속성" => "attribute",
			"*모양파일" => "shapefile",
			"*화면보정" => "screenadjust",
			"*활동시간" => "timelimit",
			"*조직" => "grouping",
			"*지능" => "intellect",
			"*품성" => "nature",
			"*시야" => "vision",
			"*생명력" => "hp",
			"*공격력" => "attack",
			"*흡수율" => "absorb",
			"*블럭율" => "block",
			"*방어력" => "defence",
			"*공격속도" => "attackspeed",
			"*명중력" => "accuracy",
			"*특수공격률" => "specattackrate",
			"*크기" => "size",
			"*공격범위" => "attackrange",
			"*생체" => "organic",
			"*번개" => "lightning",
			"*얼음" => "ice",
			"*불" => "fire",
			"*독" => "poison",
			"*매직" => "magic",
			"*몬스터종족" => "race",
			"*이동속도" => "movespeed",
			"*이동타입" => "movetype",
			"*효과음" => "soundeffect",
			"*경험치" => "exp",
			"*이벤트아이템" => "eventitem",
			"*아이템카운터" => "itemcount",
			"*아이템"=> "drops",
			"*연결파일" => "namefile"
		];
		$sizes = [
			"형" => "small",
			"소형" => "small",
			"중형" => "medium",
			"중대형" => "medium-large",
			"대형" => "large",
			"대형" => "large",
			"없음" => "none"
		];
		$races = [
			"뮤턴트" => "mutants",
			"디먼" => "demon",
			"메카닉" => "mechanic",
			"노멀" => "normal",
			"언데드" => "undead",
			"아이언" => "iron",
		];
		if (!is_file($file))
			return("Not a file...");
		$lines = file($file);
		$this->monfile = $file;
		foreach ($lines as $line)
		{
			$match = false;
			// echo "$line <br />";
			$line = mb_convert_encoding($line, "UTF-8", "EUC-KR");
			// echo "$line <br />";
			foreach ($array as $k => $v)
			{
				// echo strlen($k)." - $k => $v <br />";
				if ($match) 
					continue;
				// echo substr($line,0,strlen($k))."<br />";
				if ($v == "attribute" && substr($line,0,strlen($k)) == $k)
				{
					$match = true;
					$line = trim(str_replace($k,"",$line));
					if ($line == "적")
					{
						$this->attribute = "enemy";
					}
					else
					{
						$this->attribute = $line;
						error_log("Attribute $line  : $file");
					}
					continue;
				}
				elseif ($v == "size" && substr($line,0,strlen($k)) == $k)
				{
					$match = true;
					$line = trim(str_replace($k,"",$line));
					foreach ($sizes as $key => $value)
					{
						if ($key == $line)
						{
							$this->size = $value;
							break;
						}
					}
					//$this->size = (in_array($line, $sizes)) ? $sizes[$line] : "$line".error_log("Size $line : ".$file);
					if ($this->size == null)
					{
						error_log("Size $line : $file");
						$this->size = $line;
					}
					continue;
				}
				elseif ($v == "race" && substr($line,0,strlen($k)) == $k)
				{
					$match = true;
					$line = trim(str_replace($k,"",$line));
					foreach ($races as $key => $value)
					{
						if ($key == $line)
						{
							$this->race = $value;
							break;
						}
					}
					// $this->race = (in_array($line, $races)) ? $races[$line] : "$line".error_log("Race $line : ".$file);
					if ($this->race == null)
					{
						error_log("Race $line : $file");
						$this->race = $line;
					}
					continue;
				}
				elseif ($v == "timelimit" && substr($line,0,strlen($k)) == $k)
				{
					$match = true;
					$line = trim(str_replace("*활동시간","",$line));
					if ($line == "제한없음")
					{
						$this->timelimit = "No-Limit";
					}
					else
					{
						$this->timelimit = $line;
					}
					continue;
				}
				elseif ($v == "drops" && substr($line,0,strlen($k)) == $k && substr($line,0,strlen("*아이템카운터")) !== "*아이템카운터")
				{
					$match = true;
					$line = trim(str_replace($k,"",$line));
					if (stripos($line,"없음") !== false)
					{
						$this->nodrop = str_replace("없음","",$line);
						// $this->drops[] = str_replace("없음","No-Drop",$line);
					}
					elseif (stripos($line,"돈") !== false)
					{
						$line = trim(str_replace("돈","",$line));
						// echo $line;
						// foreach (str_split($line) as $char)
						// {
							// if ($char == "	")
								// echo "'$char' : Tab \r\n";
							// elseif ($char == " ")
								// echo "'$char' : Space \r\n";
							// else
								// echo $char. "\r\n";
						// }
						if (preg_match('/(?<rate>\d{1,6})(\s|\t)+(?<drops>[0-9\t\s]+)/',$line, $matches))
						{
							$this->moneydrop = [$matches['rate'] => $matches['drops']];
						}
						else
						{
							$this->moneydrop = $line;
						}
						// $this->drops[] = str_replace("돈","Gold",$line);
					}
					else
					{
						if (preg_match('/(?<rate>\d{1,6])\s+(?<drops>[a-zA-Z0-9\s]+)/',$line, $matches))
						{
							$this->drops[] = [[$matches['rate']] => $matches['drops']];
						}
						else
						{
							$this->drops[] = [0 => $line];
						}
					}
					continue;
				}
				elseif ($v == "boss" && substr($line,0,strlen($k)) == $k)
				{
					$this->boss = "true";
				}
				elseif (substr($line, 0, strlen($k)) == $k)
				{
					// $line = str_replace("	"," ",$line);
					$line = trim(str_replace($k,"",$line));
					$line = preg_replace("/\s+/"," ",$line);
					/*if ($v == "spec_class" || $v == "primary_spec_class")
						$line = str_replace(" ",", ",$line);
					elseif ($v !== "name")
						$line = str_replace(" "," - ",$line); */
					$this->$v = $line;
					$match = true;
					continue;
				}
			}
		}
		$this->level = ($this->level == null || $this->level == "") ? 0 : $this->level;
		$this->vision = ($this->vision == null || $this->vision == "") ? 0 : $this->vision;
		$this->hp = ($this->hp == null || $this->hp == "") ? 0 : $this->hp;
		$this->absorb = ($this->absorb == null || $this->absorb == "") ? 0 : $this->absorb;
		$this->defence = ($this->defence == null || $this->defence == "") ? 0 : $this->defence;
		$this->attackspeed = ($this->attackspeed == null || $this->attackspeed == "") ? 0 : $this->attackspeed;
		$this->accuracy = ($this->accuracy == null || $this->accuracy == "") ? 0 : $this->accuracy;
		$this->exp = ($this->exp == null || $this->exp == "") ? 0 : $this->exp;
		$this->eventitem = ($this->eventitem == null || $this->eventitem == "") ? 0 : $this->eventitem;
		$this->itemcount = ($this->itemcount == null || $this->itemcount == "") ? 0 : str_replace(" ", "", $this->itemcount);
	}
}
?>