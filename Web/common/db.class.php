<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
class dbconn {

    private $db, $user, $pass, $database, $dbconn;
    public function dbconn($dbhost, $dbuser, $dbpass, $database = null)
    {
        $this->db = $dbhost;
        $this->user = $dbuser;
        $this->pass = $dbpass;
        $this->database = $database;
    }

    public function connect() 
    {
        try 
        {
            $dbstring = ($this->database == null) ? "sqlsrv:Server={$this->db};LoginTimeout=2" : "sqlsrv:Server={$this->db};Database={$this->database};LoginTimeout=2" ;
            $this->dbconn = new PDO($dbstring, "$this->user",  "$this->pass", [
                PDO::ATTR_TIMEOUT => 5, 
                PDO::SQLSRV_ATTR_QUERY_TIMEOUT => 5, 
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::SQLSRV_ATTR_ENCODING => PDO::SQLSRV_ENCODING_UTF8]);
            return true;
        }
        catch (Exception $e)
        {
            return ['Error' => $e->getMessage()];
        }
        return false;
    }

    public function query($query)
    {
        if ($this->dbconn)
        {
            try {
                $stmt = $this->dbconn->prepare($query);
                $stmt->execute();
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
            catch (Exception $e)
            {
                error_log($query);
                error_log($e->getMessage());
            }
        }
        return false;
    }
    
    public function nonQuery($query)
    {
        if ($this->dbconn)
        {
            try {
                return $this->dbconn->exec($query);
            }
            catch (Exception $e)
            {
                error_log($query);
                error_log($e->getMessage());
            }
        }
        return false;
    }

    public function close()
    {
        $this->dbconn = null;
    }
}