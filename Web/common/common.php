<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
$_GET = filter_input_arraY(INPUT_GET, FILTER_SANITIZE_STRING);
require_once('common/uri.php');
require_once('common/extensions.php');
require_once('common/db.class.php');
require_once('common/functions.php');
if (!empty($_POST))
{
    foreach ($_POST as $k => $v)
    {
        $_POST[$k] = filterquery($v);
    }
}
$webhost = (stripos($_SERVER['SERVER_SOFTWARE'], "IIS") !== false) ? "IIS" : "Apache";
$configfile = "common/conf.ini";
$firstrun = (!file_exists($configfile)) ? true : false;
if (!$firstrun)
    $configs = parse_ini_file($configfile);
if (!is_dir('temp'))
    mkdir('temp');
if (!is_dir('temp\\sessions'))
    mkdir('temp\\sessions');
putenv('TMPDIR=temp\\');
ini_set('upload_tmp_dir', 'temp\\');
ini_set('session.save_path','temp\\sessions');
ini_set('include_path',ini_get('include_path') . ";common\\PEAR");
session_save_path('temp\\sessions');
session_start();
$classes = ["","Fighter","Mechanician","Archer","Pikeman","Atalanta","Knight","Magician","Priestess","Assassin","Shaman","Martial Artists"];
$headers = ["CONTENT" => "Content-Type: text/html; charset=utf-8",
            "LOCATION" => "Location: ", 
            "LANGUAGE" => "Content-Language: en", 
            "X-POWERED-BY" => "X-Powered-By: A fat cunt chasing a "];
$_SERVER['SERVER_SOFTWARE'] = "PT Server By Ashikabi";
if ($webhost != "IIS")
    $headers["SERVER"] = "Server: PT Server By Ashikabi";
$theme = (isset($configs['theme'])) ? $configs['theme'] : "light";
if ($page != "setup" && !$firstrun)
    require_once('login.php');
?>