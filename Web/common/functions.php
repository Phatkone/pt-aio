<?php
function email($to, $subject, $message, $messageHTML = "", $configs) {
	$messageHTML = ($messageHTML == "") ? $message : $messageHTML;
	require_once "Mail.php";
	require_once "Mail/mime.php";
	$from = $configs['sender'];
	$headers = array('From' => $from, 'To' => $to, 'Subject' => $subject);
	$smtp = Mail::factory('smtp', array(
		'host' => $configs['smtpServer'],
		'port' => $configs['smtpPort'],
		'auth' => true,
		'username' => $configs['email'],
		'password' => $configs['smtpPass']
    ));
    // Add for banner image in emails.
	// $img = dirname(__DIR__).'/images/banner.jpg';
	// $imgBase64 = base64_encode(file_get_contents($img));
	switch ($configs['theme']) 
	{
		case "dark":
			$fgc = "whitesmoke";
			$bgc = "#4b4b4b";
			$divbgc = "rgb(59, 59, 59)";
		break;
		case "light":
			$fgc = "black";
			$bgc = "#dfdfdf";
			$divbgc = "whitesmoke";
		break;
		case "contrast":
			$fgc = "black";
			$bgc = "#1a1a1a";
			$divbgc = "whitesmoke";
		break;
		default:
			$fgc = "black";
			$bgc = "#dfdfdf";
			$divbgc = "whitesmoke";
		break;
	}
	$html = "<html>
	<body style='
	background-color:{$bgc};
	width: 100%;
	height: 100%;
	padding: 30px;'>".
	//"<center><img src='cid:banner' alt='PT Banner'/></center>".
	"<div style='
		margin:0 auto;
		color: {$fgc} ;
		background-color: {$divbgc};
		width: 80%;
		padding-top: 30px;
		padding-bottom: 30px;
		padding-left: 30px;
		padding-right: 30px;'>
		$messageHTML</div>
		</body></html>";
	$mime = new Mail_mime("\r\n");
	$mime->setTXTBody($message);
	$mime->setHTMLBody($html);
    // Add for banner image in emails.
	// $mime->addAttachment($img, 'image/jpeg', $img);
	// $mime->addHTMLImage($img,'image/jpeg',$img,true,'banner');
	$body = $mime->get();
	$headers = $mime->headers($headers);
	$mail = $smtp->send($to, $headers, $body);
	if (PEAR::isError($mail))
	{
		error_log("Pear Mail Error: ".$mail->getMessage());
		return false;
	}
	else
	{
		return true;
	}
}

function accCreate($account, $password, $email, $ip, $configs) {
	$url = $configs['url'];
	$db = new dbconn($configs['dbHost'], $configs['dbUser'], $configs['dbPass']);
	if ($db->connect() !== true)
		return false;
	if ($db->query("SELECT COUNT(*) AS ROWS FROM [AccountDB].[dbo].[AllGaneUser] WHERE userid LIKE '{$account}'")[0]['ROWS'] > 0)
		return false;
	if ($configs['verification'])
    {
        $token = tokenGen();
        $emailbody = "Please verify your PT Account: $account by clicking this link: \n\r
        {$url}/?account::verify/{$account}/{$token} \n\r\n\r
                This link is valid for 24 hours.";
        $emailbodyHTML = "<h2>Please verify your PT Account: $account</h2>
            Please verify your account by clicking this link: <br /><br />
            <a style='color:orange;font-weight:bold;' href='{$url}/?account::/verify/{$account}/{$token}'>{$url}/?account::verify/{$account}/{$token}</a><br />
                This link is valid for 24 hours.<br /><br />
            <center style='color:orange;font-weight:bold;'>PT Admin Team</center>";
        if ($db->nonQuery("INSERT INTO LogDB.dbo.NewAccount 
        ([account],[password],[ipAddress],[email],[date],[token]) 
        VALUES 
        ('{$account}','{$password}','{$ip}','{$email}',GETDATE(),'$token')"))
	    {
		    if(email($email,"Please Verify Your Account $account", $emailbody, $emailbodyHTML, $configs))
		    {
	        	return true;
		    }
		    else
		    {
		    	return false;
	    	}
    	}
    	else
    	{
            return false;
        }
    }
    else
    {
        $query = "INSERT INTO [AccountDB].[dbo].[AllGameUser]
        ([userid],[Passwd],[RegistDay],[DisuseDay],[inuse],[Grade],[EventChk],[SelectChk],[BlockChk],[SpecialChk],[Credit],[DelChk],[Channel]) 
        VALUES 
        ('$account','$password',GETDATE(),'12-12-2035','0','U','0','0','0','0','0','0','$ip');";
        $table = "{$account[0]}GameUser";
        $query2 = "INSERT INTO [AccountDB].[dbo].[{$table}]
        ([userid],[Passwd],[RegistDay],[DisuseDay],[inuse],[Grade],[EventChk],[SelectChk],[BlockChk],[SpecialChk],[Credit],[DelChk],[Channel]) 
        VALUES 
        ('$account','$password',GETDATE(),'12-12-2035','0','U','0','0','0','0','0','0','$ip');";
         error_log($query2);
        if ($db->nonQuery($query) !== false && $db->nonQuery($query2) !== false)
        {
            $db->nonQuery("INSERT INTO [LogDB].[dbo].[AccountEmail] 
            ([account], [email], [ipAddress], [date]) 
            VALUES 
            ('$account','$email','$ip',GETDATE())");
            $db->nonQuery("INSERT INTO [LogDB].[dbo].[Register] (AccountName, Date, Email) VALUES ('$account', GETDATE(), (SELECT email FROM LogDB.dbo.AccountEmail e WHERE e.account = '$account'))");
            return true;
        }
    }
    return false;
}

function valid_account($account) {
	if (strlen($account) > 0)
	{
		if (ctype_alpha($account[0]))
		{
			if (ctype_alnum($account))
			{
				return true;
			}
		}
	}
	return false;
}

function valid_password($pass) {
	$valid = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()';
	$valid = str_split($valid,1);
	for ($i = 0; $i < strlen($pass); $i++)
	{
		if (!in_array($pass[$i],$valid))
		{
			return false;
		}
	}
	return true;
}

function tokenGen() {
	$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	$token = "";
	for ($i = 0; strlen($token) < 50; $i++)
	{
		$token .= $chars[rand(0,strlen($chars)-1)];
	} 
	return $token;
}

function filterQuery($query) {
    $invalid = array(';','\\');
	return(str_ireplace($invalid,' ',$query));
}

function getBodyClass() {
    $userAgent = (isset($_SERVER['HTTP_USER_AGENT'])) ? $_SERVER['HTTP_USER_AGENT'] : '-Windows';
    if (stripos($userAgent,'Android') !== false)
        return 'mobile';
    if (stripos($userAgent,'iPhone') !== false)
        return 'mobile';
    if (stripos($userAgent,'iPad') !== false)
        return 'mobile';
    if (stripos($userAgent,'Windows') !== false)
        return 'pc';
}

function popup($title, $message = "")
{
    global $configs;
	return "<div id='popup' style='width:100vw;height:100vh;position:fixed;top:0;left:0;display:none;z-index:50!important;background-color:rgba(0, 0, 0, 0.5);'>
		<div class='body' style='border:2px black solid;position:fixed;top:50%;left:50%;transform: translate(-50%, -50%);margin: 0 auto;padding:10px;z-index:50!important;width:85vw;overflow:hidden;'>
		<h2 style='margin:0 auto'>$title</h2>
		<p>$message</p>
		<br /><br />
		<button style='border-radius:1px;' id='popup-close'>OK</button></div></div>
		<script type='text/javascript'>$(document).ready(function() {
			$('#popup').show('slide',{direction:'up'}, 500);
			$('#popup-close').click( function() {
				$('#popup').hide('slide', {direction:'up'},500);
			});
        });</script>";
}

function numDir($name)
{
	$total = 0;
	for($i=0;$i<strlen($name);$i++) {
		$total+=ord(strtoupper($name[$i]));
		if($total>=256) {
			$total=$total-256;
		}
	}
	return $total;
}
?>