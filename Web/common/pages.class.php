<?php
if (!defined('PT'))
	die(header("HTTP/1.0 404 Not Found"));
class pages 
{
	public function render ($page, $ext = 'php')
	{
		if (file_exists("./web/pages/{$page}.{$ext}"))
			require_once("./web/pages/{$page}.{$ext}");
		else
			require_once("./web/pages/errorpage.php");
	}
}
?>