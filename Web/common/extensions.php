<?php
if (!defined('PT'))
    die(header("HTTP/1.0 404 Not Found"));
$extensions = ['pdo_sqlsrv', 'curl', 'PDO', 'session', 'mbstring'];
$toEnable = [];
foreach ($extensions as $extension)
{
    if (!extension_loaded($extension))
        $toEnable[] = $extension;
}
if (!empty($toEnable))
{
    $modules = implode(", ",$toEnable);
    die("
        <html>
        <head>
            <title>PristonTale Server & Site Setup</title>
        </head>
        <body style='background-color: #cdcdcd'>
            <h2 style='text-align:center'>Unable to continue</h2>
            <p style='text-align:center'>Please add on and enable the following modules and try again; <br /><br /><b>{$modules}</b></p>
        </body>
        </html>
    ");
}